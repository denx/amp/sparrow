include config.mk

SHELL := /bin/bash

ifeq ($(VERBOSE),)
SIL=@
endif

# Directories, etc
SRC_DIR  ?= src/sparrow
QCOM_DIR ?= src/qcom
GEN_DIR  ?= gen/$(PROJ)
PROJ_DIR ?= src/$(PROJ)
OBJ_DIR  ?= objs/$(PROJ)_$(DEVICE)
BIN      ?= $(DEVICE)
BIN_DIR  ?= $(DEVICE)/$(PROJ)
LINUX_OBJ_DIR := $(OBJ_DIR).linux
HWLIB_DIR ?= ./hwlib
INCLUDE_DIRS = -I $(SRC_DIR) -I $(QCOM_DIR) -I $(HWLIB_DIR)/include -I $(GEN_DIR) $(EXTRA_INCLUDES)

ifneq ($(COMPILER), ARM)
# Use the GNU compiler
CROSS_COMPILE ?= arm-altera-eabi-

CC=$(CROSS_COMPILE)gcc
CFLAGS_COMPILER =-march=armv7-a -mtune=cortex-a9 -mcpu=cortex-a9 -mfpu=neon -mfloat-abi=hard -marm -std=c99 -fno-builtin -DGNU

AS=$(CROSS_COMPILE)gcc
ASFLAGS_COMPILER:= -c $(CFLAGS_COMPILER)

LD=$(CROSS_COMPILE)gcc
LDFLAGS_COMPILER:=-nostdlib -Wl,-N,--build-id=none
LD_SET_ADDRESS=-Wl,-Ttext=
#-Wl,-e_start 

MKDIR    ?=$(SIL)mkdir -p

OBJ_DUMP ?=$(CROSS_COMPILE)objdump 
OBJ_DUMP_OPTS=-DS

OBJ_COPY ?=$(CROSS_COMPILE)objcopy
OBJ_COPY_OPTS = -O binary #Note- This must end with a space

ALT_INTERRUPT_FILES := \
	$(OBJ_DIR)/alt_interrupt.o \
	$(OBJ_DIR)/alt_interrupt_asm.o

else

# Use the ARM compiler
AS=armasm
ASFLAGS_COMPILER:=-g --cpu=Cortex-A9 --cpreproc

CC=armcc
#warning 370-D: variable "" has an uninitialized const field: Caused by socal generated .h files
CFLAGS_COMPILER	:= --cpu=Cortex-A9 --diag_suppress 370 --library_interface=aeabi_clib --c99 -DARMCC

LD=armlink
#warning 6238 - warns of aligning stack properly when calling C code from asm
LDFLAGS_COMPILER:=--entry=_start --first=_start --diag_suppress 6238 --noscanlib
LD_SET_ADDRESS=--ro-base 
MKDIR    ?=$(SIL)mkdir -p

OBJ_DUMP ?=fromelf
OBJ_DUMP_OPTS=--text -c -s -d 

OBJ_COPY ?=fromelf
OBJ_COPY_OPTS = --bin --output=

ALT_INTERRUPT_FILES := \
	$(OBJ_DIR)/alt_interrupt.o \
	$(OBJ_DIR)/alt_interrupt_armcc.o
endif

PYTHON   ?=python
PYOPTS   ?=-D $(DEVICE)
ifdef SYSTEMROOT
	MKIMAGE  ?=mkimage.exe
else
	MKIMAGE  ?=PATH=$(PATH):prebuilt mkimage
endif

PYSCRIPT ?= $(SRC_DIR)/Sparrow.py
PROJSCRIPT ?= $(PROJ_DIR)/layout.py
GEN_DEPS ?= $(GEN_DIR) $(PY_FILES) $(PYSCRIPT)

# Flags
CCFLAGS = $(CFLAGS_COMPILER) -c $(INCLUDE_DIRS) -DARM -g 
ASFLAGS = $(ASFLAGS_COMPILER) $(INCLUDE_DIRS)
LDFLAGS = $(LDFLAGS_COMPILER)
CCFLAGS += -DALT_INT_PROVISION_VECTOR_SUPPORT=1
LINUX_CFLAGS = -g -O0 -DLINUX 

ifeq ($(DEVICE), arria5)
CCFLAGS += -DARRIA5
else
CCFLAGS += -DCYCLONE5
endif

.PHONY: all
all: baremetal linux_baremetal linux_uCos2 baremetal_dual uCos2_uCos2 #test


objs =  \
	$(OBJ_DIR)/startup.o \
	$(OBJ_DIR)/reset.o \
	$(OBJ_DIR)/resetS.o \
	$(OBJ_DIR)/uart.o \
	$(OBJ_DIR)/log_buffer.o \
	$(OBJ_DIR)/alt_printf.o \
	$(OBJ_DIR)/alt_mmu.o \
	$(OBJ_DIR)/scu.o \
	$(OBJ_DIR)/scuS.o \
	$(OBJ_DIR)/mem.o \
	$(OBJ_DIR)/mempool.o \
	$(ALT_INTERRUPT_FILES) \
	$(OBJ_DIR)/gic.o \
	$(OBJ_DIR)/gicS.o \
	$(OBJ_DIR)/cache.o \
	$(OBJ_DIR)/alt_cache.o \
	$(OBJ_DIR)/cacheS.o \
	$(OBJ_DIR)/trustzone.o \
	$(OBJ_DIR)/trustzoneS.o \
	$(OBJ_DIR)/Layout.o \
	$(OBJ_DIR)/divide.o \
	$(OBJ_DIR)/neon.o

##########################################################################################################
#		Generic Rules
##########################################################################################################

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.S $(GEN_DIR)/Layout.h
	$(SIL)$(AS) $(ASFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(GEN_DIR)/Layout.h
	$(SIL)$(CC) $(CCFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(GEN_DIR)/%.c $(GEN_DIR)/Layout.h
	$(SIL)$(CC) $(CCFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(HWLIB_DIR)/src/hwmgr/%.c
	$(SIL)$(CC) $(CCFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(HWLIB_DIR)/src/hwmgr/%.s
	$(SIL)$(AS) $(ASFLAGS) -o $@ $<

$(OBJ_DIR)/%.o: $(PROJ_DIR)/%.c $(GEN_DIR)/Layout.h
	$(SIL)$(CC) $(CCFLAGS) -o $@ $< 

$(OBJ_DIR)/%.o: $(PROJ_DIR)/%.S $(GEN_DIR)/Layout.h
	$(SIL)$(AS) $(ASFLAGS) -o $@ $<

$(OBJ_DIR) $(GEN_DIR) $(BIN_DIR) $(BIN)/test/local:
	$(SIL)$(MKDIR) $@

# qcom files
$(OBJ_DIR)/%.o: $(QCOM_DIR)/%.c
	$(SIL)$(CC) $(CCFLAGS) -c $< -o $@

clean:
	$(SIL)rm -rf objs $(BIN) $(GEN_DIR) src/Sparrow.pyo

.PHONY: projfiles
projfiles: $(BIN_DIR)/sparrow.bin $(BIN_DIR)/sparrow.txt $(BIN_DIR)/sparrow.map $(BIN_DIR)/u-boot.scr

.PHONY: dumpLogScript
dumpLogScript: $(BIN_DIR)/dumpLog.sh

.PHONY: dumpLog
dumpLog: $(LINUX_OBJ_DIR)/dumpLog

########################################

CONFIG_FILE = $(PROJ_DIR)/layout.py
$(BIN_DIR)/sparrow.txt: $(BIN_DIR)/sparrow.axf
	$(SIL)$(OBJ_DUMP) $(OBJ_DUMP_OPTS) $< >$@

$(BIN_DIR)/sparrow.map: $(PROJ_DIR)/layout.py
	$(SIL)$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -p >$@

$(BIN_DIR)/bootscript.txt: $(PROJ_DIR)/layout.py
	$(SIL)$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -boots $@

$(BIN_DIR)/u-boot.scr: $(BIN_DIR)/bootscript.txt
	$(SIL)$(MKIMAGE) -A arm -O linux -T script -C none -a 0 -e 0 -n "bootscript" -d $< $@ > /dev/null

$(GEN_DIR)/Layout.c $(GEN_DIR)/Layout.h: $(GEN_DEPS) $(CONFIG_FILE)
	$(SIL)$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -C $(GEN_DIR)/Layout.c -H $(GEN_DIR)/Layout.h	


$(BIN_DIR)/sparrow.bin: $(BIN_DIR)/sparrow.axf $(CONFIG_FILE)
	$(SIL)$(OBJ_COPY) $< $(OBJ_COPY_OPTS)$(OBJ_DIR)/sparrow.raw  && \
	$(MKIMAGE) -A arm -O linux -T kernel -C none \
		-a `$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Address'` \
		-e `$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'` \
		-d $(OBJ_DIR)/sparrow.raw $@ > /dev/null && \
	chmod a+x $@
	@echo "*** Created $@ ***"

$(BIN_DIR)/dumpLog.sh: $(OBJ_DIR)
	@echo "dumpLog `$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.OS1_s_Log_Buffer.Address'` \
		`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.OS1_s_Log_Buffer.Size'`" > $@
	@chmod u+x $@

$(LINUX_OBJ_DIR)/dumpLog: src/qcom/dumpLog.c $(LINUX_OBJ_DIR)
	$(SIL)$(CC) $(LINUX_CFLAGS) -o $@ $< \
		-DLOG_ADDRESS=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.OS1_s_Log_Buffer.Address'` \
		-DLOG_SIZE=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.OS1_s_Log_Buffer.Size'`

#########################################       Baremetal App

.PHONY: baremetal
baremetal:
	@make --no-print-directory PROJ=baremetal projfiles VERBOSE=$(VERBOSE)

# baremetal app files


objs_baremetal = \
	$(OBJ_DIR)/startup.o \
	$(OBJ_DIR)/uart.o \
	$(OBJ_DIR)/log_buffer.o \
	$(OBJ_DIR)/alt_printf.o \
	$(OBJ_DIR)/alt_mmu.o \
	$(OBJ_DIR)/mem.o \
	$(OBJ_DIR)/mempool.o \
	$(OBJ_DIR)/gic.o \
	$(OBJ_DIR)/gicS.o \
	$(ALT_INTERRUPT_FILES) \
	$(OBJ_DIR)/cache.o \
	$(OBJ_DIR)/alt_cache.o \
	$(OBJ_DIR)/cacheS.o \
	$(OBJ_DIR)/Layout.o \
	$(OBJ_DIR)/trustzone.o \
	$(OBJ_DIR)/main.o \
	$(OBJ_DIR)/baremetal_app.o \
	$(OBJ_DIR)/divide.o \
	$(OBJ_DIR)/neon.o \
	$(OBJ_DIR)/alt_clock_manager.o \
	$(OBJ_DIR)/alt_timers.o \
	$(OBJ_DIR)/alt_globaltmr.o \
	$(OBJ_DIR)/alt_watchdog.o

$(BIN)/baremetal/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs_baremetal)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs_baremetal) $(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'`

#########################################       Baremetal App

objs_baremetal_dual = $(objs_baremetal) \
	$(OBJ_DIR)/scu.o \
	$(OBJ_DIR)/scuS.o \
	$(OBJ_DIR)/reset.o \
	$(OBJ_DIR)/resetS.o 

.PHONY: baremetal_dual
baremetal_dual:
	@make --no-print-directory PROJ=baremetal_dual projfiles VERBOSE=$(VERBOSE)

$(BIN)/baremetal_dual/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs_baremetal_dual)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs_baremetal_dual)  \
		$(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'` 

#########################################	AMP Linux/Baremetal App

$(LINUX_OBJ_DIR):
	$(SIL)$(MKDIR) $@

.PHONY: ledset 
ledset: $(LINUX_OBJ_DIR)/ledset

.PHONY: linux_baremetal
linux_baremetal:
	@make --no-print-directory PROJ=linux_baremetal VERBOSE=$(VERBOSE) projfiles dumpLogScript

# baremetal app files
objs_linux_baremetal = $(objs) \
		$(OBJ_DIR)/qcom.o \
		$(OBJ_DIR)/main.o \
		$(OBJ_DIR)/baremetal_app.o \
		$(OBJ_DIR)/alt_clock_manager.o \
		$(OBJ_DIR)/alt_timers.o \
		$(OBJ_DIR)/alt_globaltmr.o \
		$(OBJ_DIR)/alt_watchdog.o

# baremetal side of qcome
$(OBJ_DIR)/qcom.o: $(QCOM_DIR)/qcom.c
	$(SIL)$(CC) $(CCFLAGS) -o $@ $< \
		-DPHYS_MEM_ADDR=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
					-var 'sparrow.Shared_Comm_Channel.Address'` \
		-DMEM_SIZE=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
					-var 'sparrow.Shared_Comm_Channel.Size'`

# Linux app to talk with baremetal
$(LINUX_OBJ_DIR)/qcom.o: $(QCOM_DIR)/qcom.c $(LINUX_OBJ_DIR)
	$(SIL)$(CC) -c $(LINUX_CFLAGS) -o $@ $<

$(LINUX_OBJ_DIR)/ledset.o: src/qcom/ledset.c $(LINUX_OBJ_DIR)
	$(SIL)$(CC) -c $(LINUX_CFLAGS) -o $@ $< -Isrc/linux_baremetal \
                -DPHYS_MEM_ADDR=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.Shared_Comm_Channel.Address'` \
                -DMEM_SIZE=`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) \
                                        -var 'Linux.Shared_Comm_Channel.Size'`

$(LINUX_OBJ_DIR)/ledset: $(LINUX_OBJ_DIR)/qcom.o $(LINUX_OBJ_DIR)/ledset.o
	$(SIL)$(CC) $(CFLAGS) -o $@ $^

$(BIN)/linux_baremetal/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs_linux_baremetal)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs_linux_baremetal)  \
		$(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'` 

#########################################	linux-uCos2

.PHONY: linux_uCos2
linux_uCos2:
	@make --no-print-directory PROJ=linux_uCos2 VERBOSE=$(VERBOSE) projfiles dumpLogScript

objs_linux_ucos2 = $(objs) \
		$(OBJ_DIR)/main.o

$(BIN)/linux_uCos2/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs_linux_ucos2)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs_linux_ucos2)  \
		$(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'` 

#########################################       linux-uCos2

.PHONY: uCos2_uCos2
uCos2_uCos2:
	@make --no-print-directory PROJ=uCos2_uCos2 VERBOSE=$(VERBOSE) projfiles

objs_ucos2_ucos2 = $(objs) \
	$(OBJ_DIR)/main.o

$(BIN)/uCos2_uCos2/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs_ucos2_ucos2)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs_ucos2_ucos2)  \
		$(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'`

status:
	@echo $(CROSS_COMPILE)
	@echo $(ASFLAGS_COMPILER)
	@echo $(ASFLAGS)

#########################################       sparrowtest
LOCAL_CC = gcc

test_objs = \
        $(OBJ_DIR)/tz_gic_test.o \
        $(OBJ_DIR)/tz_sdram_test.o \
        $(OBJ_DIR)/divide_test.o \
        $(OBJ_DIR)/reset_test.o \
        $(OBJ_DIR)/print_test.o \
        $(OBJ_DIR)/mem_test.o \
        $(OBJ_DIR)/neon_test.o \
        $(OBJ_DIR)/cache_test.o \
        $(OBJ_DIR)/mempool_test.o \
        $(OBJ_DIR)/set_vbarS.o \
        $(OBJ_DIR)/set_vbar.o \
        $(OBJ_DIR)/ipc.o \
        $(OBJ_DIR)/mainS.o \
        $(OBJ_DIR)/main.o

.PHONY: test
test:
	@make --no-print-directory PROJ=test VERBOSE=$(VERBOSE) projfiles $(BIN)/test/expected_output

objs_linux_ucos2 = $(objs) \
		$(OBJ_DIR)/main.o

$(BIN)/test/sparrow.axf: $(OBJ_DIR) $(BIN_DIR) $(objs) $(test_objs)
	$(SIL)$(LD) $(LDFLAGS) -o $@ $(objs) $(test_objs) \
		$(LD_SET_ADDRESS)`$(PYTHON) $(PYSCRIPT) $(PROJSCRIPT) $(PYOPTS) -var 'sparrow.Executable.Start'`

loctest_bins= $(BIN)/test/local/print_test.o

$(BIN)/test/local/localtest: src/test/print_test.c $(BIN)/test/local
	$(SIL)$(LOCAL_CC) $< -DLINUX_PROGRAM -o $@

$(BIN)/test/expected_output: $(BIN)/test/local/localtest
	@$^ > $@

