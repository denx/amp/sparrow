/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <reset.h>
#include "ipc.h"

uint32 test_reset_cpu0()
{
  // We wait for the other cpu. If we continue 
  // then the test must have passed :)
  WaitForState(IPC_RESET_TESTS + 1);
  return 0;
}

uint32 test_reset_cpu1()
{
  SetState(IPC_RESET_TESTS + 1);
  return 0;
}
