/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

uint32 test_divide(void);
uint32 test_print(void);

uint32 test_reset_cpu0(void);
uint32 test_reset_cpu1(void);

uint32 test_mem(void);

uint32 test_mempool(void);

uint32 test_l2_cache(void);
uint32 test_l2_cache_cpu0(void);
void test_l2_cache_cpu1(void);

uint32 test_scu_cpu0(void);
void test_scu_cpu1(void);

uint32 test_tz_gic_cpu0(void);
void test_tz_gic_cpu1(void);

uint32 test_tz_sdram_rules_cpu0(void);
void test_tz_sdram_rules_cpu1(void);
