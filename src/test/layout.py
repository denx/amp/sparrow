#/******************************************************************************
#   Copyright Altera Corporation (C) 2015. All rights reserved
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms and conditions of the GNU General Public License,
#   version 2, as published by the Free Software Foundation.
#
#   This program is distributed in the hope it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along with
#   this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Based on the work of Jim Rucker at Altera Corporation
#
#******************************************************************************/

#setDevice("cyclone5")
setPhysicalMemory(1*Gb)

l2context = Context("l2 context", True, True)

NScontext = Context("nonsecure context", False, True, cpuNum=1)

MemoryRegion(
	Name = "Load Address for Sparrow",
	Size = 1*Mb,
	MapTo = [#Map(sparrow, AddrMatch=True),
		 Map(l2context, AddrMatch=True),
		 Map(NScontext, AddrMatch=True)],
	LoadFiles = [File(0x0, "sparrow.bin", "sparrow", sparrow, 0x40)],
	StackFor = cpu0,
	MemFor = cpu0)

MemoryRegion(
	Name = "Cache Test MR",
	Size = 1*Mb,
	Address = 0x0,
	MapTo = [ Map(l2context, NoCache=True, Device=True),
		  Map(l2context, Shared=True) ])

MemoryRegion(
	Name = "Stack for cpu 1",
	Size = 1*Mb,
	MapTo = [Map(sparrow, AddrMatch=True),
		 Map(l2context, AddrMatch=True)],
	StackFor = 1)

MemoryRegion(
	Name = "MemPool Test MR",
	Size = 1*Mb)

MemoryRegion(
	Name = "MemLog Test MR",
	Size = 1*Mb)

MemoryRegion(
        Name = "TZ SDRamRule Sec MR",
        Size = 1*Mb)

MemoryRegion(
        Name = "TZ SDRamRule NS MR",
        Size = 1*Mb)

MemoryRegion(
        Name = "TZ SDRamRule Both MR",
        Size = 1*Mb)

MemoryRegion(
        Name = "TZ SDRamRule Neither MR",
        Size = 1*Mb)

MemoryRegion(
        Name    = "Peripherals",
        Address = 0xFF200000,
        Size    = 0x00E00000,
        MapTo   = [Map(l2context, Device=True, NoCache=True, AddrMatch=True)])

sparrow.setMapped(True)
