/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>

/*    Created as a global so that the compiler doesn't optimize away    */
uint32 u32c[] = {0x80000001u, 1, 0x80000000u};
int32  i32c[] = {-3, 2, 3, -2};
uint64 u64c[] = {0x8000000000000001u, 1, 0x8000000000000000u};
int64  i64c[] = {-3, 2, 3, -2};

uint32 test_divide()
{
  uint32 failed_flag = 0;
  if(u32c[0]/u32c[1] != 0x80000001u)
      failed_flag |= 1<<0;
  if(u32c[0]%u32c[1] != 0)
      failed_flag |= 1<<1;
  if(u32c[0]/u32c[2] != 1)
      failed_flag |= 1<<2;
  if(u32c[0]%u32c[2] != 1)
      failed_flag |= 1<<3;

  if(i32c[0]/i32c[1] != -1)
      failed_flag |= 1<<4;
  if(i32c[0]%i32c[1] != -1)
      failed_flag |= 1<<5;
  if(i32c[2]/i32c[3] != -1)
      failed_flag |= 1<<6;
  if(i32c[2]%i32c[3] != 1)
      failed_flag |= 1<<7;

  if(u64c[0]/u64c[1] != 0x8000000000000001u)
      failed_flag |= 1<<16;
  if(u64c[0]%u64c[1] != 0)
      failed_flag |= 1<<17;
  if(u64c[0]/u64c[2] != 1)
      failed_flag |= 1<<18;
  if(u64c[0]%u64c[2] != 1)
      failed_flag |= 1<<19;

  if(i64c[0]/i64c[1] != -1)
      failed_flag |= 1<<20;
  if(i64c[0]%i64c[1] != -1)
      failed_flag |= 1<<21;
  if(i64c[2]/i64c[3] != -1)
      failed_flag |= 1<<22;
  if(i64c[2]%i64c[3] != 1)
      failed_flag |= 1<<23;
  return failed_flag;
}
