/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <reset.h>
#include "set_vbar.h"

#define STACK_SIZE 1024
static uint32 abort_stack[STACK_SIZE];
static uint32 irq_stack[STACK_SIZE];
static uint32 undef_stack[STACK_SIZE];

static void null_handler(void)
{
}


void setupInts(handler *p_gic_handler, handler *p_abort_handler, handler *p_undef_handler, handler *p_swi_handler)
{
  if(p_gic_handler == NULL)   p_gic_handler = null_handler;
  if(p_abort_handler == NULL) p_abort_handler = null_handler;
  if(p_undef_handler == NULL) p_undef_handler = null_handler;
  if(p_swi_handler == NULL)   p_swi_handler = null_handler;

  undef_handler = p_undef_handler;
  swi_handler = p_swi_handler;
  irq_handler = p_gic_handler;
  firq_handler = p_gic_handler;
  data_abort_handler = p_abort_handler;
  prefetch_abort_handler = p_abort_handler;

  set_abort_stack(&abort_stack[STACK_SIZE]);
  set_irq_stack(&irq_stack[STACK_SIZE]);
  set_firq_stack(&irq_stack[STACK_SIZE]);
  set_undef_stack(&undef_stack[STACK_SIZE]);

  set_vbar(test_interrupts);
}

void setTestVbar(void)
{
  set_vbar(&test_interrupts);
}
