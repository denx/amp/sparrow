/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#ifdef LINUX_PROGRAM
#include <stdio.h>
typedef unsigned int uint32;

static uint32 test_print_to(FILE *to);
void main(void)
{
  test_print_to(stdout);
}
#else
#include <core.h>
#include <stdio.h>
#include <log_buffer.h>
#include "Layout.h"
static uint32 test_print_to(FILE *to);

char ResultsBuffer[] = "**** Begining print_test ****\n"
            "printing %d: 12345678\n"
            "printing %d: -12345678\n"
            "printing %u: 2147483648\n"
            "printing %x: 1234abcd\n"
            "printing %X: 1234ABCD\n"
            "printing %c: #\n"
            "printing %s: Test\n"
            "printing %p: 0x1234abcd\n"
            "printing %8x:        3\n"
            "printing %*x:     2\n"
            "printing %#x: 0x4\n"
            "printing %+d: +5\n"
            "printing %-8x: a       \n"
            "printing %.8x: 00000009\n"
            "printing %.*x: 00000a\n"
            "printing percent: %\n"
            "printing tab(    )\n"
            "printing punctuation: ~!@#$^&*()_+:\";',./`-={}|[]\\<>? \n";

bufferInfo _LOG0;
FILE *LOG0=(FILE *)&_LOG0;

uint32 test_print(void)
{
  uint32 retval, count;
  char *SysLog = (char*)System_MemLog_Test_MR_Address;
  init_log_buffer(LOG0, (void *)System_MemLog_Test_MR_Address, System_MemLog_Test_MR_Size);
  retval = test_print_to(LOG0);
  for(count=0; count < sizeof(ResultsBuffer)-1; count++)
  {
    if(SysLog[count] != ResultsBuffer[count])
    {
      printf("Test buffers = %p, %p\n", SysLog, ResultsBuffer);
      printf("Print Test error offset = 0x%x\n", count);
      retval |= 1u<<31;
      break;
    }
  }
  return retval;
}
#endif

static uint32 test_print_to(FILE *to)
{
  uint32 retval = 0;
  if(30 != fprintf(to, "**** Begining print_test ****\n"))
    retval |= 1<<0;
  if(22 != fprintf(to, "printing %%d: %d\n", 12345678))
    retval |= 1<<1;
  if(23 != fprintf(to, "printing %%d: %d\n", -12345678))
    retval |= 1<<2;
  if(24 != fprintf(to, "printing %%u: %u\n", 0x80000000u))
    retval |= 1<<3;
  if(22 != fprintf(to, "printing %%x: %x\n", 0x1234ABCD))
    retval |= 1<<4;
  if(22 != fprintf(to, "printing %%X: %X\n", 0x1234ABCD))
    retval |= 1<<5;
#if defined(PRINT_FLOATS)
  //printf("printing %%f: %f\n", 12.34f);
  //printf("printing %%F: %F\n", 12.34);
  //printf("printing %%e: %e\n", 12.34f);
  //printf("printing %%E: %E\n", 12.34);
  //printf("printing %%g: %g\n", 12.34f);
  //printf("printing %%G: %G\n", 12.34);
  //printf("printing %%a: %a\n", 12.34f);
  //printf("printing %%A: %A\n", 12.34);
#endif
  if(15 != fprintf(to, "printing %%c: %c\n", '#'))
    retval |= 1<<6;
  if(18 != fprintf(to, "printing %%s: %s\n", "Test"))
    retval |= 1<<7;
  if(24 != fprintf(to, "printing %%p: %p\n", (void *)0x1234abcd))
    retval |= 1<<8;

  if(23 != fprintf(to, "printing %%8x: %8x\n", 3))
    retval |= 1<<9;
  if(20 != fprintf(to, "printing %%*x: %*x\n", 5, 2))
    retval |= 1<<10;
  if(18 != fprintf(to, "printing %%#x: %#x\n", 4))
    retval |= 1<<11;
  if(17 != fprintf(to, "printing %%+d: %+d\n", 5))
    retval |= 1<<12;
  if(24 != fprintf(to, "printing %%-8x: %-8x\n", 0xa))
    retval |= 1<<13;
  if(24 != fprintf(to, "printing %%.8x: %.8x\n", 0x9))
    retval |= 1<<14;
  if(22 != fprintf(to, "printing %%.*x: %.*x\n", 0x6, 0xA))
    retval |= 1<<15;

  if(20 != fprintf(to, "printing percent: %%\n"))
    retval |= 1<<16;
  if(16 != fprintf(to, "printing tab(\t)\n"))
    retval |= 1<<17;
  if(55 != fprintf(to, "printing punctuation: ~!@#$^&*()_+:\";',./`-={}|[]\\<>? \n"))
    retval |= 1<<18;

  return retval;
}

