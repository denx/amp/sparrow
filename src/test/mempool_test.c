/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <stdio.h>
#include "core.h"
#include "mempool.h"
#include "Layout.h"

uint32 test_mempool(void)
{
  uint32 retval = 0;
  uint32 count;
  MemPool test_pool;
  MemPoolInit(&test_pool,
        System_MemPool_Test_MR_Address,
        System_MemPool_Test_MR_Size);

  for(count=0; count< 256; count++)
  {
    uint32 loc = (uint32) MemPoolAlloc(4096, &test_pool);
    if(!loc)// Check for success
    {  retval = 1<<0; break;}

    if(loc & 4095)// Check alignment on 4k
    {  retval = 1<<1; break;}

    // Check that alloced mem is within pool range
    if(loc < System_MemPool_Test_MR_Address ||
       loc > System_MemPool_Test_MR_Address + 
             System_MemPool_Test_MR_Size)
    {  retval = 1<<2; break;}
  }
  if(count != 256)
      printf("Count that failed: %d (%x)\n", count, count);
  return retval;
}
