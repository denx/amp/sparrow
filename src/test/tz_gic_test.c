/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <alt_interrupt.h>
#include "ipc.h"
#include "set_vbar.h"


static uint32 gic_count;
static void gic_handler(void)
{
  gic_count++;
}

static uint32 abort_count;
static void abort_handler(void)
{
  abort_count++;
}

static uint32 retval = 0;

uint32 test_tz_gic_cpu0()
{
  WaitForState(IPC_TZ_GIC_TESTS + 1);
  return retval;
}

uint32 test_tz_gic_cpu1()
{
  int count;
  retval = 0;

  // Setup handlers
  setupInts(gic_handler, abort_handler, NULL, NULL);
  setTestVbar();
  gic_count = abort_count = 0;

  // Can we change the gic?
  for(count=32; count<256; count++)
  {
    alt_int_cpu_target_t val;
    alt_int_dist_target_set((ALT_INT_INTERRUPT_t) count, (alt_int_cpu_target_t) 1<<1);
    alt_int_dist_target_get((ALT_INT_INTERRUPT_t) count, &val);
    if(val == 1<<1)
      retval |= 1;
  }

  // Can we trigger a secure int? If so, fail
  for(count=32; count<256; count++)
  {
    alt_int_dist_pending_set((ALT_INT_INTERRUPT_t) count);
  }
  if(gic_count != 0)
    retval |= 2;
  SetState(IPC_TZ_GIC_TESTS + 1);
  return retval;
}

