/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

typedef void handler(void);

extern handler *undef_handler;
extern handler *swi_handler;
extern handler *prefetch_abort_handler;
extern handler *irq_handler;
extern handler *firq_handler;
extern handler *data_abort_handler;

void set_undef_stack(void *stackend);
void set_abort_stack(void *stackend);
void set_irq_stack(void *stackend);
void set_firq_stack(void *stackend);

// test_interrupts is the value you pass to set_vbar
extern uint32 *test_interrupts;


void setupInts(handler *p_gic_handler, handler *p_abort_handler, handler *p_undef_handler, handler *p_swi_handler);
void setTestVbar(void);

uint32 alt_mmu_disable2(void);

