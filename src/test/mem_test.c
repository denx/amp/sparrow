/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <mem.h>

char buffer[128];

uint32 test_mem()
{
  int a;
  int retval = 0;
  char result1[8] = {'a', 'a', 'a', 't', 't', 'a', 'a', 'a'};
  char result2[8] = {'a', 0, 0, 0, 0, 0, 0, 'a'};
  // Test memset
  {
    memset(buffer, 'a', 8);
    memset(buffer+3, 't', 2);
    for(a=0; a<8; a++)
      if(result1[a] != buffer[a])
        retval |= 1<< a;
  }
  // Test memzero
  {
    memzero(buffer+1, 6);
    for(a=0; a<8; a++)
      if(result2[a] != buffer[a])
        retval |= (1<<8) << a;
  }
  // Test memchr
  {
    buffer[100] = 'z';
    if(memchr(buffer, 'z', 8) != NULL)
      retval |= 1<<16;
    if(memchr(buffer, 'a', 8) != buffer)
      retval |= 1<<17;
    if(memchr(buffer+1, 'a', 7) != &buffer[7])
      retval |= 1<<18;
    if(memchr(buffer, 0, 8) != &buffer[1])
      retval |= 1<<19;
    
  }
  // Test memcheck
  {
    if(memcheck(buffer, 0, 8) != buffer)
      retval |= 1<<20;
    if(memcheck(buffer, 'a', 8) != buffer+1)
      retval |= 1<<21;
    if(memcheck(buffer+1, 0, 7) != &buffer[7])
      retval |= 1<<22;
    if(memcheck(buffer+1, 0, 6) != NULL)
      retval |= 1<<23;
  }
  // Test memcpy
  {
    if(&buffer[8] != memcpy(&buffer[8], buffer, 8))
      retval |= 1<<24;
    // Copy 0-7 to 16-23
    for(a=0; a<8; a++)
      if(buffer[8+a] != result2[a])
        retval |= 1<<25;
    // Copy 1-6 to 25-30 (Memory notaligned on 32 bit boundry)
    if(&buffer[25] != memcpy(&buffer[25], &buffer[1], 6))
      retval |= 1<<26;
    for(a=0; a<6; a++)
      if(buffer[25+a] != result2[1+a])
        retval |= 1<<27;

    // Copy 1-6 to 32 (Source unaligned, Destination aligned)
    if(&buffer[32] != memcpy(&buffer[32], &buffer[1], 6))
      retval |= 1<<28;
    for(a=0; a<5; a++)
      if(buffer[32+a] != result2[1+a])
        retval |= 1<<29;
    // Copy 0-6 to 51 (Source aligned, Destination unaligned)
    memcpy(&buffer[51], &buffer[0], 6);
    for(a=0; a<6; a++)
      if(buffer[51+a] != result2[a])
        retval |= 1<<30;
    // Copy 3-7 to 65-70 (Source & Destination unaligned, different boundries)
    memcpy(&buffer[65], &buffer[3], 5);
    for(a=0; a<5; a++)
      if(buffer[65+a] != result2[3+a])
        retval |= 1u<<31;
  }
  return retval;
}

