/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <alt_interrupt.h>
#include <trustzone.h>
#include "ipc.h"
#include "set_vbar.h"
#include "Layout.h"
#include <stdio.h>

static uint32 gic_count;
static void gic_handler(void)
{
  gic_count++;
}

static uint32 abort_count;
static void abort_handler(void)
{
  abort_count++;
}

static uint32 AccessMem(uint32 address)
{
  static uint32 ssum = 0;
  abort_count = 0;
  ssum += *((uint32 *)address);
  return abort_count;
}

static uint32 retval1 = 0;

uint32 test_tz_sdram_rules_cpu0()
{
  uint32 count;
  uint32 retval0 = 0;
  uint32 rulenums[5] = {
            ALT_SDR_CTL_RULE_NEW,
            ALT_SDR_CTL_RULE_NEW,
            ALT_SDR_CTL_RULE_NEW,
            ALT_SDR_CTL_RULE_NEW,
            ALT_SDR_CTL_RULE_NEW};

  setupInts(gic_handler, abort_handler, NULL, NULL);
  setTestVbar();


  if(0 != AccessMem(System_TZ_SDRamRule_NS_MR_Address))// Should work
    printf("P-\n");

// Set a rule for all the test code to be accessible
  rulenums[0] = alt_sdr_ctl_set_tz_rule(ALT_SDR_CTL_RULE_NEW,
        System_Load_Address_for_Sparrow_Address, 
        System_Load_Address_for_Sparrow_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_BOTH, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_ALLOW_ACCESS);

  rulenums[1] = alt_sdr_ctl_set_tz_rule(ALT_SDR_CTL_RULE_NEW,
        System_Stack_for_cpu_1_Address,
        System_Stack_for_cpu_1_AddrEnd,
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_BOTH, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_ALLOW_ACCESS);

// Test Set 1 - Set Default to allow access
  alt_sdr_ctl_set_tz_default(ALT_SDR_CTL_PROTPORT_DENY_NONE);

  rulenums[2] = alt_sdr_ctl_set_tz_rule(ALT_SDR_CTL_RULE_NEW,
        System_TZ_SDRamRule_Sec_MR_Address, 
        System_TZ_SDRamRule_Sec_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_SECURE, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_DENY_ACCESS);
  rulenums[3] = alt_sdr_ctl_set_tz_rule(ALT_SDR_CTL_RULE_NEW,
        System_TZ_SDRamRule_NS_MR_Address, 
        System_TZ_SDRamRule_NS_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_NONSECURE, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_DENY_ACCESS);
  rulenums[4] = alt_sdr_ctl_set_tz_rule(ALT_SDR_CTL_RULE_NEW,
        System_TZ_SDRamRule_Both_MR_Address, 
        System_TZ_SDRamRule_Both_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_BOTH, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_DENY_ACCESS);
  // Test Secure access
  if(0 != AccessMem(System_TZ_SDRamRule_NS_MR_Address))// Should work
    retval0 |= 1<<0;
  if(0 == AccessMem(System_TZ_SDRamRule_Sec_MR_Address))// Should fail
    retval0 |= 1<<1;
  if(0 == AccessMem(System_TZ_SDRamRule_Both_MR_Address))// Should fail
    retval0 |= 1<<2;
  if(0 != AccessMem(System_TZ_SDRamRule_Neither_MR_Address))// Should work
    retval0 |= 1<<3;

  // Have CPU1 (NS) test them
  SetState(IPC_TZ_SDRAM_TESTS + 2);

  // Wait for tests on CPU1 complete
  WaitForState(IPC_TZ_SDRAM_TESTS + 3);

// Test Set 2 - Set Default to deny access
  alt_sdr_ctl_set_tz_rule(rulenums[2],
        System_TZ_SDRamRule_Sec_MR_Address, 
        System_TZ_SDRamRule_Sec_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_SECURE, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_ALLOW_ACCESS);
  alt_sdr_ctl_set_tz_rule(rulenums[3],
        System_TZ_SDRamRule_NS_MR_Address, 
        System_TZ_SDRamRule_NS_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_NONSECURE, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_ALLOW_ACCESS);
  alt_sdr_ctl_set_tz_rule(rulenums[4],
        System_TZ_SDRamRule_Both_MR_Address, 
        System_TZ_SDRamRule_Both_MR_AddrEnd, 
        ALT_SDR_CTL_RULEID_MIN, ALT_SDR_CTL_RULEID_MAX,
        ALT_SDR_CTL_DATA_ACCESS_BOTH, ALT_SDR_CTL_DATA_RULE_ALL_PORTS, 
        ALT_SDR_CTL_DATA_ALLOW_ACCESS);
  alt_sdr_ctl_set_tz_default(ALT_SDR_CTL_PROTPORT_DENY_CPU);

  // Test Secure access
  
  if(0 == AccessMem(System_TZ_SDRamRule_NS_MR_Address))// Should fail
    retval0 |= 1<<8;
  if(0 != AccessMem(System_TZ_SDRamRule_Sec_MR_Address))// Should pass
    retval0 |= 1<<9;
  if(0 != AccessMem(System_TZ_SDRamRule_Both_MR_Address))// Should pass
    retval0 |= 1<<10;
  if(0 == AccessMem(System_TZ_SDRamRule_Neither_MR_Address))// Should fail
    retval0 |= 1<<11;

  // Have CPU1 (NS) test them
  SetState(IPC_TZ_SDRAM_TESTS + 4);

  // Wait for tests on CPU1 complete
  WaitForState(IPC_TZ_SDRAM_TESTS + 5);

  alt_sdr_ctl_set_tz_default(ALT_SDR_CTL_PROTPORT_DENY_NONE);

  for(count=0; count<5; count++)
    if(rulenums[count] != ALT_SDR_CTL_RULE_NEW)
      alt_sdr_ctl_delete_rule(rulenums[count]);

  return retval0 | retval1;
}

void test_tz_sdram_rules_cpu1()
{
  setTestVbar();
  retval1 = 0;
// Test Set 1 - Set Default to allow access
  WaitForState(IPC_TZ_SDRAM_TESTS + 2);
  // Test NS access
  abort_count = 0;
  if(0 == AccessMem(System_TZ_SDRamRule_NS_MR_Address))// Should fail
    retval1 |= 1<<16;
  if(0 != AccessMem(System_TZ_SDRamRule_Sec_MR_Address))// Should work
    retval1 |= 1<<17;
  if(0 == AccessMem(System_TZ_SDRamRule_Both_MR_Address))// Should fail
    retval1 |= 1<<18;
  if(0 != AccessMem(System_TZ_SDRamRule_Neither_MR_Address))// Should work
    retval1 |= 1<<19;

  SetState(IPC_TZ_SDRAM_TESTS + 3);
// Test Set 2 - Set Default to deny access
  WaitForState(IPC_TZ_SDRAM_TESTS + 4);
  // Test NS access
  // Test Secure access
  if(0 != AccessMem(System_TZ_SDRamRule_NS_MR_Address))// Should work
    retval1 |= 1<<24;
  if(0 == AccessMem(System_TZ_SDRamRule_Sec_MR_Address))// Should fail 
    retval1 |= 1<<25;
  if(0 != AccessMem(System_TZ_SDRamRule_Both_MR_Address))// Should work 
    retval1 |= 1<<26;
  if(0 == AccessMem(System_TZ_SDRamRule_Neither_MR_Address))// Should fail
    retval1 |= 1<<27;

  SetState(IPC_TZ_SDRAM_TESTS + 5);
}



uint32 test_peripheral_access()
{ return 0;}
