/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <core.h>
#include <neon.h>
#include "set_vbar.h"

uint32 g_undef_count;
void neon_undef_handler(void)
{
  g_undef_count++;
}

uint32 test_neon()
{
  uint32 r0;
  uint32 retval = 0;
  g_undef_count = 0;

  setupInts(NULL, NULL, neon_undef_handler, NULL);

  // Perform neon instruction
  __asm("vmrs    r0, fpscr");
  if(g_undef_count != 1)
    retval |= 1;
  __enable_neon();
  // Perform neon instruction
  __asm("vmrs    r0, fpscr");
  if(g_undef_count != 1)
    retval |= 2;
  return retval;
}
