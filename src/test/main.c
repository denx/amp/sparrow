/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <stdio.h>
#include <core.h>
#include <reset.h>
#include <trustzone.h>
#include "Layout.h"
#include "test.h"
#include "ipc.h"

// Forward Declaration of Tests

void cpu1_main(void);

int cpu0_main(void)
{
  int retval;
  if(0 != (retval = test_print()))
    printf("*ERROR* Print Test Failed: %08x\n", retval);
  else
    printf("Print Test Passed\n");

  if(0 != (retval = test_divide()))
    printf("*ERROR* Divide Test Failed: %08x\n", retval);
  else
    printf("Divide Test Passed\n");

  if(0 != (retval = test_mem()))
    printf("*ERROR* Mem Test Failed: %08x\n", retval);
  else
    printf("Mem Test Passed\n");

  if(0 != (retval = test_mempool()))
    printf("*ERROR* MemPool Test Failed: %08x\n", retval);
  else
    printf("MemPool Test Passed\n");

  if(0 != (retval = test_l2_cache()))
    printf("*ERROR* L2 Cache Test Failed: %08x\n", retval);
  else
    printf("L2 Cache Test Passed\n");

// Tests involving 2 CPUs
  InitIPC();
  wakeup_CPU(1, cpu1_main);

  if(0 != (retval = test_reset_cpu0()))
    printf("*ERROR* Reset Test Failed: %08x\n", retval);
  else
    printf("Reset Test Passed\n");

  if(0 != (retval = test_l2_cache_cpu0()))
    printf("*ERROR* Dual Core L2 Cache Test Failed: %08x\n", retval);
  else
    printf("Dual Core L2 Cache Test Passed\n");

  if(0 != (retval = test_scu_cpu0()))
    printf("*ERROR* SCU Test Failed: %08x\n", retval);
  else
    printf("SCU Test Passed\n");

  if(0 != (retval = test_tz_gic_cpu0()))
    printf("*ERROR* TZ Gic Test Failed: %08x\n", retval);
  else
    printf("TZ Gic Test Passed\n");

  if(0 != (retval = test_tz_sdram_rules_cpu0()))
    printf("*ERROR* TZ SDRam Test Failed: %08x\n", retval);
  else
    printf("TZ Gic SDRam Passed\n");

  printf("Tests Complete\n");
  while(1)
    wait_for_event();
}

extern uint32 cpu1_tz_start;

void cpu1_main(void)
{
  test_reset_cpu1();
  test_l2_cache_cpu1();
  test_scu_cpu1();
  start_nonsecure((uint32)&cpu1_tz_start, cpu1_TZ_Stack_End);
  while(1)
    wait_for_event();
}

void cpu1_main_nonsecure(void)
{
  test_tz_gic_cpu1();
  test_tz_sdram_rules_cpu1();
}

void smc_handler(int Reason)
{

}

int main(void){}
