/******************************************************************************

   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include <stdio.h>
#include "core.h"
#include "Layout.h"
#include "reset.h"
#include "cache.h"
#include "gic.h"
#include "alt_interrupt.h"

#include "trustzone.h"
#include "mempool.h"
#include "mem.h"

#define VERSION "1.0"

void cpu1_main(void);

int cpu0_main(void)
{
  // Were going to set up the L2 cache here before the BM apps are run

  // CPU0 instructions will into WAY0-3
  alt_cache_l2_set_locks(CPU0_INSTRUCTION | CPU0_DATA, WAY0_3);

  // CPU1 instructions will into WAY0-3
  alt_cache_l2_set_locks(CPU1_INSTRUCTION | CPU1_DATA, WAY4_7);

  // enable l2 cache but not the L1
  alt_cache_l2_init();
  alt_cache_l2_prefetch_enable();
  alt_cache_l2_parity_enable();
  alt_cache_l2_enable();

  // Note: Although the l2 cache is enabled, instructions and data will not be cached unless
  // they are in virtual memory with the pages marked as cachable. When the MMU is turned off
  // it will not store any entries to the l2 cache.

  printf("Sparrow v "VERSION"\n");

  SetupTZPeripherals();

  wakeup_CPU(1, cpu1_OS_Start);

  SetupTZMemAccess();

  printf("Starting uCos2 on Core 0....\n\n");

  cpu0_OS_Start();

  return 0;
}

void smc_handler(int Reason)
{
}

#ifndef GNU
int main(void)
{ return 0;}
#endif
