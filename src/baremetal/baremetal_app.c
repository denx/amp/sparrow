/* -*- linux-c -*-
   ****************************************************************************

   Copyright Altera Corporation (C) 2015. All rights reserved
   Copyright (C) 2015 Pavel Machek, Denx

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

    /* standard includes */
#include <stdint.h>
#include <stdio.h>
#include "core.h"
#include "mem.h"

    /* SoCAL layer includes*/
#include    "socal/hps.h"
#include    "socal/socal.h"
#include    "socal/alt_rstmgr.h"
#include    "socal/alt_gpio.h"
#include    "socal/alt_tmr.h"

    /* Hardware Libraries includes */
#include    "alt_clock_manager.h"
#include    "alt_timers.h"
#include    "alt_globaltmr.h"
#include    "alt_watchdog.h"
#include    "alt_mpu_registers.h"
#include    "alt_interrupt.h"
#include    "Layout.h"
#include    "leds.h"

#define OSCTMR0  2

    /* some timer & LWED enables */

#define USE_IRQ
#define MY_CPU 0
#define NO_LINUX

uint32_t TimerCounts[4] = {0,0,0,0};

#include "../race/timing.c"

int32_t alt_amp_MPU1_osc1tmr0_setup(void)
{
	ALT_STATUS_CODE ret = ALT_E_ERROR;
	uint32_t temp;

	alt_clrbits_word(ALT_RSTMGR_PERMODRST_ADDR, ALT_RSTMGR_PERMODRST_OSC1TMR0_SET_MSK);
	ret = alt_clk_freq_get(ALT_CLK_L3_MP, &temp);
	if (ret == ALT_E_SUCCESS)
	{
		int delay = 0x02FAF080;
		printf("OSC1 Input Frequency = %u\n", (unsigned)temp);
		ret = __alt_amp_MPU1_osc1tmr0_setup(delay);
		printf("Delay = %d\n", delay);
		temp = alt_gpt_time_millisecs_get(delay);
		printf("OSC1 Timer 0 triggers each %u mS\n", (unsigned)temp);
		//ret = alt_gpt_int_enable(ALT_GPT_OSC1_TMR0);
	}
	return 0;
}


int main(void)
{
	// initialize ports necessary
	// set up interrupts for reading messages & sensing inputs from FPGA
	// set up timer to trigger @ 1 sec - use to toggle LED
	TimerCounts[0] = TimerCounts[1] = TimerCounts[2] = TimerCounts[3] = 0;

	// setup gpio to toggle LED
	alt_gpt_int_disable(ALT_GPT_OSC1_TMR0);
	alt_gpt_tmr_stop(ALT_GPT_OSC1_TMR0);

	// start the private timer
	alt_amp_MPU1_osc1tmr0_setup();

	main_loop();
}
