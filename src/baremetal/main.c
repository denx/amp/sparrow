/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <stdio.h>
#include "core.h"
#include "cache.h"
#include "neon.h"
#include "Layout.h"

int main(void);

int cpu0_main(void)
{
  printf("Baremetal app v 1.2 \n");  
  alt_cache_system_enable();

  // Note: Although the cache is enabled, instructions and data will not be cached unless
  // they are in virtual memory with the pages marked as cachable. When the MMU is turned off
  // it will not store any entries to the cache.

  printf("cache ok\n");

  // Set up the MMU
  Map_sparrow();

  __enable_neon();

  return main();
}

