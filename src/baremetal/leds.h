/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#ifdef ARRIAV
#define     ALT_AMP_DEMO_MPU1_LED0_MSK      0x00000001
#define     ALT_AMP_DEMO_MPU1_LED1_MSK      0x00000800
#define     ALT_AMP_DEMO_MPU1_LED2_MSK      0x00020000
#define     ALT_AMP_DEMO_MPU1_LED3_MSK      0x00040000

#define     alt_toggle_LED0()               alt_xorbits_word(ALT_GPIO0_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED0_MSK)
#define     alt_toggle_LED1()               alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED1_MSK)
#define     alt_toggle_LED2()               alt_xorbits_word(ALT_GPIO0_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED2_MSK)
#define     alt_toggle_LED3()               alt_xorbits_word(ALT_GPIO0_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED3_MSK)
#define     alt_toggle_all_LEDs()           alt_xorbits_word(ALT_GPIO0_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED0_MSK \
	                                            | ALT_AMP_DEMO_MPU1_LED2_MSK | ALT_AMP_DEMO_MPU1_LED3_MSK); \
                                            alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED1_MSK)
#else
#define     ALT_AMP_DEMO_MPU1_LED0_MSK      0x00008000
#define     ALT_AMP_DEMO_MPU1_LED1_MSK      0x00004000
#define     ALT_AMP_DEMO_MPU1_LED2_MSK      0x00002000
#define     ALT_AMP_DEMO_MPU1_LED3_MSK      0x00001000

#define     alt_toggle_LED0()               alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED0_MSK)
#define     alt_toggle_LED1()               alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED1_MSK)
#define     alt_toggle_LED2()               alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED2_MSK)
#define     alt_toggle_LED3()               alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED3_MSK)
#define     alt_toggle_all_LEDs()           alt_xorbits_word(ALT_GPIO1_SWPORTA_DR_ADDR, ALT_AMP_DEMO_MPU1_LED0_MSK \
                                            | ALT_AMP_DEMO_MPU1_LED1_MSK | ALT_AMP_DEMO_MPU1_LED2_MSK \
                                            | ALT_AMP_DEMO_MPU1_LED3_MSK)
#endif

