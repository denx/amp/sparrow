/* -*- linux-c -*-
 * Copyright 2015,2016 Pavel Machek <pavel@denx.de>
 *
 * GPLv2
 */

uint32_t seed = 123456789;

#ifndef LINUX
#ifdef USE_IRQ
#define MARK "Baremetal/irq"
#else
#define MARK "Baremetal/poll"
#endif
#else
#define MARK "Linux"
#endif

static long int random(void)
{
	int a = 1103515245;
	int c = 12345;

	seed = (a * seed + c);
	return seed;
}

#define RESOLUTION 100
static int histogram[RESOLUTION];
static int limits[RESOLUTION] = { 20, 21, 23, 24, 26, 28, 30, 33, 35, 38, 41, 44, 47, 51, 55, 59, 63, 68, 73, 78, 84, 91, 97, 105, 113, 121, 130, 140, 150, 162, 174, 187, 201, 216, 232, 249, 268, 288, 309, 332, 357, 384, 413, 444, 477, 512, 551, 592, 636, 684, 735, 789, 848, 912, 980, 1053, 1132, 1216, 1307, 1405, 1510, 1622, 1744, 1874, 2014, 2164, 2326, 2499, 2686, 2887, 3102, 3334, 3583, 3850, 4138, 4447, 4779, 5135, 5519, 5931, 6374, 6849, 7361, 7910, 8501, 9136, 9818, 10551, 11338, 12185, 13095, 14072, 15123, 16252, 17465, 18769, 20170, 21676, 23295, 25034 };

static int record(int32_t delay)
{
#if 1
#define	TIMER 500000
#define TIMER_MIN 200000
#define EVERY 1000
#else
#define	TIMER 1
//#define TIMER_MIN 100000000 /* Once in four seconds */
#define TIMER_MIN 25000000 /* Once in second */
#define EVERY 1
#endif
#define SMOOTH 100 /* If set too high, rounding errors will corrupt the result */
	static int count;
	static int total;
	static int min = 999999999;
	static int max = 0;
	static int max10 = 0;	
	static int invalid;
	static int moving = 300;
	int i;

	if (delay < min)
		min = delay;
	if (delay > max)
		max = delay;
	if (count > 10)
		if (delay > max10)
			max10 = delay;

	if (delay > 0) { 
		count += 1;
		total += (delay * 10);
		moving = ((SMOOTH-1) * moving + (delay * 100) + 50) / SMOOTH;
	} else
		invalid += 1;

	for (i=0; i<RESOLUTION-1; i++)
		if (delay < limits[i])
			break;
	histogram[i]++;

	if (!(count % EVERY)) {
		if (invalid)
			printf("Invalid %d. ", invalid);
		printf("This %d, # %d, min %d, avg %d.%d, moving avg %d.%d, max %d max10 %d\n",
		       delay, count, min,
		       (total/count)/10, (total/count)%10,
		       moving/100, (moving/10)%10,
		       max, max10);
		if (!(count % (EVERY * 10))) {
			int sum = 0;
			printf("Histogram ");
			for (i=0; i<RESOLUTION; i++) {
				sum += histogram[i];
				printf("%d, ", histogram[i]);
			}
			printf("%d\n", sum);			
		}
	}
	
	return 0;
}

static unsigned long get_next(void)
{
	unsigned long r = random();
	r = r % TIMER + TIMER_MIN;
	//printf("next: %d\n", r);
	return r;
}

#ifndef LINUX
int32_t __alt_amp_MPU1_osc1tmr0_setup(int32_t delay)
{
  ALT_STATUS_CODE ret;
  ret = alt_gpt_int_disable(ALT_GPT_OSC1_TMR0);
  ret = alt_gpt_counter_set(ALT_GPT_OSC1_TMR0, delay);
  ret = alt_gpt_mode_set(ALT_GPT_OSC1_TMR0, ALT_GPT_RESTART_MODE_ONESHOT);
  ret = alt_gpt_tmr_start(ALT_GPT_OSC1_TMR0);
  return ret;
}

static int handle_race(void)
{
	int32_t now;

	now = alt_gpt_counter_get(ALT_GPT_OSC1_TMR0);
	now = -now;
	record(now);

	unsigned long r =get_next();
	__alt_amp_MPU1_osc1tmr0_setup(r);
}

bool alt_amp_osc1tmr0_update(void)
{
	bool ret = 0;

	ret = alt_read_word(ALT_OSC1TMR0_TMRSRAWINTSTAT_ADDR) & ALT_TMR_TMRSINTSTAT_TMRSINTSTAT_SET_MSK;

	if (ret)
	{
		TimerCounts[OSCTMR0]++;
		//alt_toggle_LED2();
		handle_race();		
		alt_gpt_int_clear_pending(ALT_GPT_OSC1_TMR0);
	}

	return ret;
}

void my_irq(uint32_t icciar, void * context)
{
	int e;

	//printf("got irq, stopping\n");
	alt_amp_osc1tmr0_update();
	alt_gpt_tmr_start(ALT_GPT_OSC1_TMR0);
	e = alt_gpt_int_enable(ALT_GPT_OSC1_TMR0);
}

static void enable_irq(int irq)
{
	ALT_STATUS_CODE e;
  
	printf("isr register %d\n", irq);
	e = alt_int_isr_register(irq, my_irq, NULL);
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);    

	e = alt_int_dist_enable(irq);
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);
	e = alt_int_dist_target_set(irq, 1 << MY_CPU);
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);

	e = alt_int_dist_trigger_set(irq, ALT_INT_TRIGGER_LEVEL);
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);
}

void wait_for_linux(void)
{
	volatile int i, j;

	for (j=0; j<3; j++) {
		printf("Waiting for Linux to boot... %d\n", j);
		for (i=0; i<1000000000; i++) {
			i++;
		}
	}
	printf("Linux should be ready by now.\n");
}

void setup_irqs(void)
{
	ALT_STATUS_CODE e;

	printf("intializing interrupts\n");
	e = alt_int_global_init();
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);
	
	e = alt_int_cpu_init();
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);	

#ifndef NO_LINUX
	wait_for_linux();
#endif
	enable_irq(201);				
    
	printf("enable\n");
	e = alt_int_global_enable();
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);

	printf("step2: enable\n");
	e = alt_int_cpu_enable();
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);	
	printf("polling\n");

	/* Neccessary? */

	(void) alt_read_word(ALT_OSC1TMR0_TMRSEOI_ADDR);
	alt_gpt_tmr_start(ALT_GPT_OSC1_TMR0);

	e = alt_gpt_int_enable(ALT_GPT_OSC1_TMR0);
	printf("res = %d / OK=%d\n", e, ALT_E_SUCCESS);        
}

void main_loop(void)
{
	uint32_t    exit = 0;
	
#ifdef USE_IRQ
	setup_irqs();
	/*
	  ICCICR . E = 1 ... enable interrupts alt_int_cpu_enable()
	  ICCPMR ... priority mask, alt_int_cpu_priority_mask_set()
	  	... called from alt_int_cpu_init();
	  ICCIAR ... tells me interrupt #, alt_int_handler_irq()
	  ICDDCR . E = 1 ... alt_int_global_enable()
	  ICDISER[n] ...  alt_int_dist_enable()
	  ICDIPTR ...  alt_int_dist_target_set()
	  ICDICFR .. . alt_int_dist_trigger_set( , ALT_INT_TRIGGER_LEVEL);
	*/
	
	{
		static volatile int i = 0, j = 0;
		while (1) {
			if (i < 1000) {
				__asm__ __volatile__("wfi");				
				i++;
				continue;
			}
			i = 0;
			printf("Still going -- %d\n", j++);
		}
	}
#else
	while (exit == 0)
	{

		// check for any timeout from global timer
		alt_amp_osc1tmr0_update();

	}
#endif	
}

#endif
