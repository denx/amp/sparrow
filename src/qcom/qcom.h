/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#ifndef QCOM_H
#define QCOM_H

typedef void *channel_handle;


#define CHANNEL_TYPE_FIXED              0
#define CHANNEL_TYPE_VARIABLE           1
uint32_t create_channel(channel_handle *ch, uint32_t type, uint32_t msg_size, 
	uint32_t physaddr, uint32_t size);
uint32_t destroy_channel(channel_handle ch);

// 0 copy interface - only use if speed is crucial!
uint32_t zero_getbuff(channel_handle ch, char **buff, uint32_t *size);
uint32_t zero_postbuff(channel_handle *ch);
uint32_t zero_pollbuff(channel_handle *ch, char **buff, uint32_t *size);
uint32_t zero_buffdone(channel_handle *ch);

// Simple message interface
uint32_t post_message(channel_handle *chan, char *buff, uint32_t size);
uint32_t poll_message(channel_handle *chan, char *buff, uint32_t *size);

extern int q_reversed;
#endif

