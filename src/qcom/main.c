/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include "qcom.h"


int main(int argc, char **argv)
{
  channel_handle ch;
  void * mem;
  uint32_t *p_magic;
  unsigned int send, recv;
  unsigned int send_size, recv_size;
  int num_messages = 1;
  int count = 1;
  char *message = "Hello";
  char buffer[256];
  uint32_t size = 256;
  uint32_t memrgn_address= PHYS_MEM_ADDR, memrgn_size = MEM_SIZE;

  for(;count < argc; count++)
  {
    if(!strcmp(argv[count], "-r"))
    {
      q_reversed = 1;
      continue;
    }
    if(!strcmp(argv[count], "-n"))
    {
      count++;
      num_messages = atoi(argv[count]);
      continue;
    }
    if(!strcmp(argv[count], "-a"))
    {
      if(argc < argc + 3)
      {
        printf("-a option requires <address> and <size>\n");
        return -1;
      }
      memrgn_address = strtol(argv[count+1]);
      memrgn_size = strtol(argv[count+2]);
      count+=2;
      continue;
    }
    message = argv[count];
  }

  if(create_channel(&ch, CHANNEL_TYPE_FIXED, 252, memrgn_address, memrgn_size))
    return 1;

  for(count = 0; count < num_messages; count++)
  {
    post_message(ch, message, 1 + strlen(message));
  }
  while(1)
  {
    size = 256;
    if(0 == poll_message(ch, buffer, &size))
      printf("Msg recvd (%d bytes): %s\n", size, buffer);fflush(stdout);
  }
  destroy_channel(ch);
  return 0;
}

