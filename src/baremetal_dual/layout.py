#   Copyright Altera Corporation (C) 2015. All rights reserved
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms and conditions of the GNU General Public License,
#   version 2, as published by the Free Software Foundation.
#
#   This program is distributed in the hope it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along with
#   this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Based on the work of Jim Rucker at Altera Corporation
#
#####################			 Sample Script for Baremetal App
setDevice("cyclone5")
setPhysicalMemory(1*Gb)

MemoryRegion(
	Name = "Memory for Baremetal App",
	Address = 0x0, # u-boot cannot load to address 0
	MapTo = [Map(sparrow, AddrMatch=True)],
	LoadFiles = [File(0x100, "sparrow.bin" , "sparrow", sparrow, 0x40)],
	StackFor = cpu0,
	MemFor = cpu0)

MemoryRegion(
	Name = "Stack for cpu 1",
	Size = 1*Mb,
	MapTo = [Map(sparrow)],
	StackFor = 1)

MemoryRegion(
	Name    = "Peripherals",
	Address = 0xFF200000,
	Size    = 0x00E00000,
	MapTo   = [Map(sparrow, Device=True, NoCache=True, AddrMatch=True)])

sparrow.setMapped(True)
