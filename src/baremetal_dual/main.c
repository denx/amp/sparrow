/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include <stdio.h>
#include "mempool.h"
#include "core.h"
#include "scu.h"
#include "reset.h"
#include "cache.h"
#include "neon.h"
#include "Layout.h"

int main(void);
void cpu1_main(void);

uint32_t *tb;

int cpu0_main(void)
{
  MemPool pool;

  printf("Baremetal app v 1.1 \n");

  alt_cache_system_enable();

  enable_smp();
  enable_scu();

  // Code copied from generated Layout.c, modified to be used by both cores
  MemPoolInit(&pool,
        sparrow_TLB_for_sparrow_Address,
        sparrow_TLB_for_sparrow_Size);

  alt_mmu_va_space_create(&tb, sparrow_regions, sizeof(sparrow_regions)/sizeof(sparrow_regions[0]), MemPoolAlloc, &pool);

  // This wakeup call should be done before the mmu is enabled because it requires writes not be cached
  wakeup_CPU(1, cpu1_main);

  alt_mmu_DACR_set(g_domain_ap, 16);
  alt_mmu_TTBCR_set(true, false, 0);
  alt_mmu_TTBR0_set(tb);
  alt_mmu_enable();

  __enable_neon();

  while(1)
    __asm__( "wfi" );
}

void cpu1_main(void)
{
  printf("CPU1 Online");

  // Note: Although the cache is enabled, instructions and data will not be cached unless
  // they are in virtual memory with the pages marked as cachable. When the MMU is turned off
  // it will not store any entries to the cache.

  enable_smp();

  alt_mmu_DACR_set(g_domain_ap, 16);
  alt_mmu_TTBCR_set(true, false, 0);
  alt_mmu_TTBR0_set(tb);
  alt_mmu_enable();
  __enable_neon();

  main();
}

