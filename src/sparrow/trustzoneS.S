/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include "asm.h"
	AREA(Trustzone, CODE, READONLY, ALIGN=5)
	EXPORT(start_nonsecure)	/*(cpu%d_StartAddr, Stack Addr)*/
	IMPORT(smc_handler)
	IMPORT(u_boot_params)

LABEL(start_nonsecure)
	push	{r0,r1}
	bl      set_mvbar
	bl      _set_nsacr
	
	/* Call Monitor Mode */
	pop	{r0,r1}
	SMC     #1

/************************************
*       _set_nsacr
************************************/
LABEL(_set_nsacr)
        /* Allow NS-Access to Coprocessors (NSACR) */
        ldr     r1, =(0x30c00)  /* NSACR */
        MCR     p15, 0, r1, c1, c1, 2
        bx      lr

/************************************
*	set_mvbar
************************************/
	EXPORT(set_mvbar)
LABEL(set_mvbar)
	/* Set the MVBar */
	adr 	r0, _mvbar_init
LABEL(really_set_mvbar)
	MCR 	p15, 0, r0, c12, c0, 1 /* Write Rt to MVBar */
	bx 	lr

/************************************
*	_mvbar - monitor vector
************************************/
	ALIGN32
LABEL(_mvbar)
	nop				/* Unused */
	nop				/* Unused */
	adr	pc, _smc_os		/* Call _smc_os */
	adr	pc, _smc_loop		/* Prefetch Abort */

	adr	pc, _smc_loop		/* Data Abort */
	nop				/* Unused */
	adr	pc, _smc_loop		/* IRQ */
	adr	pc, _smc_loop		/* Fast IRQ */

/* This is the mvbar the first time it is called (transition to non-secure mode)*/
LABEL(_mvbar_init)
	nop				/* Unused */
	nop				/* Unused */
	adr	pc, _smc_startup	/* smc */
	adr	pc, _smc_loop		/* Prefetch Abort */

	adr	pc, _smc_loop		/* Data Abort */
	nop				/* Unused */
	adr	pc, _smc_loop		/* IRQ */
	adr	pc, _smc_loop		/* Fast IRQ */

LABEL(_smc_loop)
	nop
	b	_smc_loop

/* This is the smc handler which is called by a non-secure OS */
LABEL(_smc_os)
	/*	save state */
	stmfd	sp!,{r0-r4,lr}

	bl	smc_handler

	ldmfd	sp!,{r0-r4,pc}^

/* Param 0 - Jump to address
   Param 1 - Stack End */
LABEL(_smc_startup)
	mov	r5, r0
	/* set up the SMC stack to param #1 */
	mov	sp, r1

	/* step 1 - replace this SMC with a new one in case it's called from non-secure mode */
	adr	r0, _mvbar
	bl	really_set_mvbar

	/* Change to run-secure mode */
	mov     r1, #0x31
	MCR     p15, 0, r1, c1, c1, 0 /* Write Rt to SCR */

/* Setup return cpsr */
	mrs	r0, cpsr
	/* Remove the 'monitor' mode */
	bic	r0, r0, #0x1f
	/* Add 'svc' mode */
	orr	r0, r0, #19
	push 	{r0}
	
	ldr	r6, =(u_boot_params)
	ldmia	r6, {r0,r1,r2,r3,r12,lr}

	/* Set Address of Address of next instruction */
	push	{r5}
	RFEIA   sp!

	END
