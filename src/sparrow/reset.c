/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <stdint.h>
#include "core.h"
#include "reset.h"
#include "socal/hps.h"
#include "socal/alt_rstmgr.h"

volatile int g_cpuX_state = 0;
reset_function *g_cpuX_main_func = 0;
uint32 g_cpuX_Stack_End = 0;


extern uint32 resetvector;
extern uint32 cpu_stack_addrs[];
volatile uint32 * const reset_mpumodrst = (uint32 *) (ALT_RSTMGR_OFST + ALT_RSTMGR_MPUMODRST_OFST);

#define MAX_CPUS 2
#define RESET_FUNCTION_SIZE 2
uint32 interrupt_save_buffer[RESET_FUNCTION_SIZE];

void unblock_CPU0(void)
{
  // Unblock CPU 0
  g_cpuX_state = 1;
}

void wakeup_CPU(uint32 cpuNum, reset_function *call_function)
{
  uint32 resetbits = 1<<cpuNum;

  if(cpuNum >= MAX_CPUS)
    return;

  // Step 1 - reset our jump vector
  uint32 c;
  uint32 *interrupt_table_loc = (uint32 *) 0; // It is literally at address 0
  uint32 *interrupt_table_from = &resetvector;

  // Setup global variables
  g_cpuX_state = 0; 
  g_cpuX_main_func = call_function;
  g_cpuX_Stack_End = cpu_stack_addrs[cpuNum];

  // Saving whatever was at address 0 in case it was important
  for(c=0 ; c < RESET_FUNCTION_SIZE; c++)
  {
    interrupt_save_buffer[c] = interrupt_table_loc[c];
    interrupt_table_loc[c] = interrupt_table_from[c];
  }

  // Step 2 - reset CPU 1
  *reset_mpumodrst &= ~resetbits;

  // Step 3 - Wait for CPU 1 to wake up before overwriting address 0
  while(g_cpuX_state == 0)
    ;

  // Step 4 - Restoring whatever was at address 0
  for(c=0 ; c < RESET_FUNCTION_SIZE; c++)
  {
    interrupt_table_loc[c] = interrupt_save_buffer[c];
  }
}

void reset_CPU(uint32 cpuNum)
{
  uint32 resetbits = 1<<cpuNum;
  *reset_mpumodrst |= resetbits;
}
