/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "asm.h"
	AREA(Reset, CODE, READONLY)
	EXPORT(cpuX_start)
	EXPORT(resetvector)
	EXPORT(wait_for_event)
	EXPORT(wait_for_interrupt)
	IMPORT(g_cpuX_Stack_End)
	IMPORT(gic_init_cpu)
	IMPORT(unblock_CPU0)
	IMPORT(g_cpuX_main_func)

LABEL(cpuX_start)
        /* Set up Stack for CPU 1 */
	ldr	sp, =(g_cpuX_Stack_End)
	ldr	sp, [sp]
        bl      unblock_CPU0
        bl      gic_init_cpu
        ldr     r0, =g_cpuX_main_func
        ldr     r0, [r0]
        cmp     r0, #0
        movne   pc, r0
LABEL(loop2)
        wfi
        b       loop2
	DCD	cpuX_start

LABEL(resetvector)
        ldr     pc, [pc, #-4]
        DCD   cpuX_start

LABEL(wait_for_event)
	wfe
	bx	lr

LABEL(wait_for_interrupt)
	wfi
	bx	lr

	END
