/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "core.h"
#include "socal/hps.h"


#define ALT_MPUSCU_CNTRL_OFFSET           0x0
#define ALT_MPUSCU_CNTRL_ENABLE           0x1
#define ALT_MPUSCU_CNTRL_ADDR_FILTER      0x2
#define ALT_MPUSCU_CNTRL_RAM_PARITY       0x4
#define ALT_MPUSCU_CNTRL_SPEC_LINEFILL    0x8
#define ALT_MPUSCU_CNTRL_PORT0_EN         0x10
#define ALT_MPUSCU_CNTRL_SCU_STANDBY_EN   0x20
#define ALT_MPUSCU_CNTRL_IC_STANDBY_EN    0x40

#define ALT_MPUSCU_INVALIDATE_OFFSET      0xC
#define ALT_MPUSCU_INVALIDATE_ALL         0x0000FFFF
void enable_scu(void)
{
  volatile uint32 *pCntrl = (uint32 *)((uint32) ALT_MPUSCU_ADDR + ALT_MPUSCU_CNTRL_OFFSET);
  volatile uint32 *pInvalidate = (uint32 *)((uint32)ALT_MPUSCU_ADDR + ALT_MPUSCU_INVALIDATE_OFFSET);

  *pCntrl |= ALT_MPUSCU_CNTRL_SPEC_LINEFILL | ALT_MPUSCU_CNTRL_ENABLE;
  *pInvalidate = ALT_MPUSCU_INVALIDATE_ALL;
  
}
