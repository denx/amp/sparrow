/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include "core.h"
#include "uart.h"

#include <stdint.h>
#include "alt_printf.h"
#include "socal/hps.h"
#include "socal/alt_uart.h"

FILEOP term0_st = {uart_putchar, (void *)ALT_UART0_OFST};
FILEOP term1_st = {uart_putchar, (void *)ALT_UART1_OFST};

int init_uart1(void)
{
  // UART0 has already set up by uboot
  // UART1 Reset
  ALT_UART_raw_t *pSerial = (ALT_UART_raw_t *) ALT_UART1_OFST;
  pSerial->_u_0x8.fcr =
    // Reset Receive
    ALT_UART_FCR_RFIFOR_SET(ALT_UART_FCR_XFIFOR_E_RST) |
    // Reset Send
    ALT_UART_FCR_XFIFOR_SET(ALT_UART_FCR_XFIFOR_E_RST);
  // Set Data Len  to 8
  pSerial->lcr = ALT_UART_LCR_DLS_E_LEN8;
  pSerial->mcr =
    // Set Data Terminal Ready
    ALT_UART_MCR_DTR_SET(ALT_UART_MCR_DTR_E_LOGIC0) |
    // Set Send Ready
    ALT_UART_MCR_RTS_SET(ALT_UART_MCR_RTS_E_LOGIC0);
  return 0;
}

void uart_putchar(void *port, char toprint)
{
  ALT_UART_raw_t *pSerial = (ALT_UART_raw_t *) port;
  // While Ready is 0
  while(ALT_UART_LSR_THRE_GET(pSerial->lsr) == 0)
    ;
  pSerial->rbr_thr_dll = ALT_UART_RBR_THR_DLL_VALUE_SET(toprint);

  if(toprint == '\n')
    uart_putchar(port, '\r');
}

int fgetch(void *port)
{
  ALT_UART_raw_t *pSerial = (ALT_UART_raw_t *) port;
  while(ALT_UART_LSR_DR_GET(pSerial->lsr) == ALT_UART_LSR_DR_E_NODATARDY)
    ;
  return ALT_UART_RBR_THR_DLL_VALUE_GET(pSerial->rbr_thr_dll);
}

int getch(void)
{
  return fgetch((void *)ALT_UART0_OFST);
}
