/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include "gic.h"
#include "Layout.h"
#include "alt_interrupt.h"

void gic_init_dist(void)
{
  cpu_disable_ints();
  alt_int_global_init();
  // Enable Secure and Nonsecure interrupts
  alt_int_global_enable_all();
  SetupTZInterrupts();
}

void gic_init_cpu(void)
{
  alt_int_cpu_init();
  alt_int_cpu_config_set(1, 0, 1);
  alt_int_cpu_enable_all();
}


