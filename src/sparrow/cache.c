/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "core.h"
#include "cache.h"
#include "socal/hps.h"

#define ALT_MPUL2_D_LOCKDOWN0_OFST          0x900
#define ALT_MPUL2_I_LOCKDOWN0_OFST          0x904
#define ALT_MPUL2_D_LOCKDOWN1_OFST          0x908
#define ALT_MPUL2_I_LOCKDOWN1_OFST          0x90c
#define ALT_MPUL2_SYNC                      0x730
#define ALT_MPUL2_CLEAN                     0x7bc
#define ALT_MPUL2_INVALIDATE                0x77c
#define ALT_MPUL2_CLEAN_INVALIDATE          0x7fc

#define NUM_LOCK_TYPES 4
struct _cpu_lut
{
// Cpu # + data or instruction. Consts are defined such as CPU0_DATA in .h file
  uint32 cpu_di; 
  volatile uint32 *offset;
} cpus[NUM_LOCK_TYPES] = {   
        {CPU0_DATA, 
          (volatile uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_D_LOCKDOWN0_OFST)},
        {CPU0_INSTRUCTION, 
          (volatile uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_I_LOCKDOWN0_OFST)},
        {CPU1_DATA, 
          (volatile uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_D_LOCKDOWN1_OFST)},
        {CPU1_INSTRUCTION, 
          (volatile uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_I_LOCKDOWN1_OFST)}};

void alt_cache_l2_set_locks( uint32 cpus_di, uint32 ways)
{
  uint32 a;
  for(a = 0; a < NUM_LOCK_TYPES; a++)
  {
    if(cpus[a].cpu_di & cpus_di)
    {
      volatile uint32 *l2_reg = cpus[a].offset;
      *l2_reg = ways;
    }      
  }
}

uint32 *p_CacheSync = (uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_SYNC);
uint32 *p_CacheCleanByWay = (uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_CLEAN);
uint32 *p_CacheCleanInvalidateByWay = (uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_CLEAN_INVALIDATE);
uint32 *p_CacheInvalidateByWay = (uint32 *)(ALT_MPUL2_OFST + ALT_MPUL2_INVALIDATE);

void alt_cache_l2_clean_cache_by_way( uint32 ways)
{
  // Clean the caches
  *p_CacheCleanByWay = ways;
  // Need to wait for completion
  while(*p_CacheCleanByWay & ways)
    ;
}

void alt_cache_l2_clean_invalidate_cache_by_way( uint32 ways)
{
  // Invalidate the caches
  *p_CacheCleanInvalidateByWay = ways;
  // Need to wait for completion
  while((*p_CacheCleanInvalidateByWay) & ways)
    ;
}

void alt_cache_l2_invalidate_cache_by_way( uint32 ways)
{
  // Invalidate the caches
  *p_CacheInvalidateByWay = ways;
  // Need to wait for completion
  while(*p_CacheInvalidateByWay & ways)
    ;
}

