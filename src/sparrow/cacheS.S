/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "asm.h"

    AREA(cacheS, CODE, READONLY)

    EXPORT(alt_cache_l2_preload_instructions)
LABEL(alt_cache_l2_preload_instructions)
/* r0 = memory location
   r1 = size	*/
        cmp	r1, #0
	ble	done
	sub	r1, #4
	pli	[r0, r1]
	b	alt_cache_l2_preload_instructions
LABEL(done)
	mov	pc, lr

    EXPORT(alt_cache_l2_load2ways)
LABEL(alt_cache_l2_load2ways)
/* r0 address
   r1 size
   r2 way
   clear this memory from cache */
#if 0
        push	{r3, r4}
	mov	r3, #(0xFFFEF7F0)
 disable the l1 d cache
	mrc	p15, 0, r5, c1, c0, 0
	bic	r4, r5, #1	/* save r5 for end of function */
	mcr	p15, 0, r4, c1, c0, 0
#endif
	add	r1,r1,r0	/* r1 = addr end */
/* set the cache for data only */
	ldr	r3, =(0xFFFEF90c)
	ldr	r4, =(0xFFFF)
	str	r4, [r3]	/* 0xFFFEF90c = CPU1 I */
	sub	r3,r3,#4
	str	r4, [r3]	/* 0xFFFEF908 = CPU1 D */
	sub	r3,r3,#4
	str	r4, [r3]	/* 0xFFFEF904 = CPU0 I */
	sub	r3,r3,#4
	eor	r2,r2,r4
	str	r2, [r3]	/* 0xFFFEF900 = CPU0 D */
	dmb
	isb
/* load each value */
LABEL(load_cache_loop)
	ldr	r2, [r0]	/* Load memory to cache */
/*	str	r2, [r0]	*//* Store it back to get past l1 */
	add	r0, #4
	cmp	r0,r1
	bne	load_cache_loop

/*	mcr	p15, 0, r5, c1, c0, 0 */
	str	r4, [r3]	/* 0xFFFEF900 = CPU0 D */
	bx	lr
    END
