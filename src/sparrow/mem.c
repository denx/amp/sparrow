/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "core.h"

void *memset(void *s, char c, size_t n)
{
    size_t a;
    unsigned char *s_char = (unsigned char *)s;
    for(a = 0; a<n; a++)
        s_char[a] = (char) c;
    return s;
}

void memzero(void *s, size_t n)
{
    memset(s, 0, n);
}

// Returns first instance of c
void *memchr(void *s, char c, size_t n)
{
    size_t a;
    unsigned char *s_char = (unsigned char *)s;
    for(a = 0; a < n; a++)
        if(s_char[a] == c)
            return &s_char[a];
    return NULL;
}

// Returns first instance of non-c
void *memcheck(void *s, char c, size_t n)
{
    size_t a;
    unsigned char *s_char = (unsigned char *)s;
    for(a = 0; a < n; a++)
        if(s_char[a] != c)
            return &s_char[a];
    return NULL;
}

/**********************************************************************/
void *memcpy(void *to, const void *from, size_t size)
{
  char *_to = (char *)to;
  const char *_from = (const char *)from;

  size_t a;
  if(from == NULL || to == NULL)
     return 0;

  for(a = 0; a<size; a++)
  {
    *(_to++) = *(_from++);
  }
  return to;
}

/**********************************************************************/
#ifdef ARMCC
void __aeabi_memset8(void *dest, size_t n, int c)
{
  memset(dest, c, n);
}
void __aeabi_memset4(void *dest, size_t n, int c)
{
  memset(dest, c, n);
}
void __aeabi_memset(void *dest, size_t n, int c)
{
  memset(dest, c, n);
}
void __aeabi_memclr8(void *dest, size_t n)
{
  memset(dest, 0, n);
}
void __aeabi_memclr4(void *dest, size_t n)
{
  memset(dest, 0, n);
}
void __aeabi_memclr(void *dest, size_t n)
{
  memset(dest, 0, n);
}
void __aeabi_memcpy (void *dest, const void *src, size_t n)
{
  memcpy (dest, src, n);
}
void __aeabi_memcpy8 (void *dest, const void *src, size_t n)
{
  memcpy (dest, src, n);
}
void __aeabi_memcpy4 (void *dest, const void *src, size_t n)
{
  memcpy (dest, src, n);
}
#endif

