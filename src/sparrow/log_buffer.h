/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#ifndef LOG_BUFFER
#define LOG_BUFFER

typedef struct bufferInfo_t
{
  void (*print_function)(void *context, char toprint);
  void *context;
  char *buffer;
  uint32 size;
  uint32 *p_offset;
} bufferInfo;

uint32 init_log_buffer(FILE *buffInfo, void *location, uint32 size);
void buffer_putchar(void *port, char toprint);
#endif
