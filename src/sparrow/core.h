/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#if !defined(CORE_H)
#define CORE_H
#include <stdarg.h>

typedef int int32;
typedef unsigned int uint32;
typedef unsigned int size_t;
typedef unsigned int native_int;

typedef long long int int64;
typedef unsigned long long int uint64;

#if !defined(NULL)
#define NULL	((void*)0)
#endif

#endif //CORE_H
