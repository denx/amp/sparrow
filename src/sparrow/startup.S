/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include "asm.h"
#include "Layout.h"

    IMPORT(memset)
    IMPORT(gic_init_dist)
    IMPORT(gic_init_cpu)
    IMPORT(cpu0_main)
    IMPORT(||Image$$ZI$$Base||)
    IMPORT(||Image$$ZI$$Limit||)

    AREA(Startup, CODE, READONLY, ALIGN=5)
    ENTRY
    EXPORT(_start)
LABEL(_start) /* 40 */
/* Need to set SP_MON and LR_MON */
	adr	r5, u_boot_params
	stmia	r5, {r0,r1,r2,r3,r12,lr}
	ldr	sp, =(cpu0_Stack_End)
	/* Call memset(__bss_start__,0,__bss_end__-__bss_start__) */
#ifdef GNU
	ldr     r0, =__bss_start__
	ldr     r2, =__bss_end__
#else
	ldr	r0, =||Image$$ZI$$Base||
	ldr	r2, =||Image$$ZI$$Limit||
#endif
	mov	r1, #0
	sub	r2,r2,r0
	bl	memset

	bl	gic_init_dist
	bl	gic_init_cpu
	b	cpu0_main

    EXPORT(u_boot_params)
LABEL(u_boot_params)
	DCD	0,0,0,0,0,0,0,0


/************************************
*	_interrupts - int vector
************************************/
    ALIGN32
    EXPORT(_interrupts)
LABEL(_interrupts)
	ldr	pc, [pc, #24]	/* reset */
	ldr	pc, [pc, #24]	/* undefined instruction */
	ldr	pc, [pc, #24]	/* swi */
	ldr	pc, [pc, #24]	/* prefetch abort */

	ldr	pc, [pc, #24]	/* Data Abort */
	DCD	0		/* Unused */
	ldr	pc, [pc, #24]	/* IRQ */
	ldr	pc, [pc, #24]	/* Fast IRQ */
	
	DCD	_iloop
	DCD	_iloop
	DCD	_iloop
	DCD	_iloop

	DCD	_iloop
	DCD	_iloop
	DCD	_iloop
	DCD	_iloop
    EXPORT(__main)
LABEL(__main) /* 40 */
LABEL(_iloop)
	nop
	nop
	nop
	b	_iloop

    EXPORT(_interrupts_end)
LABEL(_interrupts_end)

/************************************
	set_vbar
************************************/

     EXPORT(set_vbar)
LABEL(set_vbar)
	/* Set the VBar */
	mcr 	p15, 0, r0, c12, c0, 0
	bx	lr


/* Space set aside for storing command line options from uboot */
     EXPORT(start_secure)	/* (cpu%d_StartAddr) */
LABEL(start_secure)
	mov	pc,r0

    END
