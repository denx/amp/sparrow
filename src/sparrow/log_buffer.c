/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include <stdio.h>
#include "core.h"
#include "log_buffer.h"

#define LOG_MAGIC    0x12348765

uint32 init_log_buffer(FILE *fileinfo, void *location, uint32 size)
{
  bufferInfo *pbufferInfo = (bufferInfo *)fileinfo;
  pbufferInfo->print_function = buffer_putchar;
  pbufferInfo->context = pbufferInfo;
  pbufferInfo->buffer = location;
  pbufferInfo->size = size;
  pbufferInfo->p_offset = (uint32 *)((uint32)location + size - 2*sizeof(uint32));
  pbufferInfo->p_offset[0] = 0;
  pbufferInfo->p_offset[1] = LOG_MAGIC;
  return 0;
}

void buffer_putchar(void *bInfo, char toprint)
{
  bufferInfo *pbufferInfo = (bufferInfo *) bInfo;

  if(*pbufferInfo->p_offset >= pbufferInfo->size)
    return;//Full
  pbufferInfo->buffer[(*pbufferInfo->p_offset)++] = toprint;
}

