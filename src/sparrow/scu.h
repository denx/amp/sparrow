/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

// Each processor must call enable_smp to join the SMP sharing domain. The processors in this domain will share their branch prediction buffers, instruction caches, and TLB maintenance information. This should only be done if these processors will likely share memory mappings and execute code in the same memory (such as an SMP OS)
void enable_smp(void);

// This function needs to only be called once. It will enable the SCU which will maintain data cache coherency, manage ACP accesses, and arbitrate accesses to the L2
void enable_scu(void);
