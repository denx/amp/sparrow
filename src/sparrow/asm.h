/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#define ASM

#ifdef GNU
#define IMPORT(x)
#define EXPORT(x)       .globl  x
#define AREA(...)
#define ENTRY
#define END
#define LABEL(x)        x:
#define DCD             .word
#define _OR_		|
#define ALIGN32		.align	5
#else
#define IMPORT(x)       IMPORT  x
#define AREA(...)       AREA    __VA_ARGS__
#define EXPORT(x)       EXPORT  x
#define LABEL(x) x
#define _OR_		:OR:
#define ALIGN32		ALIGN 32
#endif

