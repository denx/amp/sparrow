/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#if !defined(CACHE_H)
#define CACHE_H
#include "alt_cache.h"

// L2 cache 
#define WAY0  1
#define WAY1  2
#define WAY2  4
#define WAY3  8
#define WAY4  0x10
#define WAY5  0x20
#define WAY6  0x40
#define WAY7  0x80
#define WAY0_3 0x0F
#define WAY4_7 0xF0
#define WAY0_7 0xFF

#define CPU_NONE          0

#define CPU0_DATA         1
#define CPU0_INSTRUCTION  2
#define CPU0              3

#define CPU1_DATA         4
#define CPU1_INSTRUCTION  8
#define CPU1              12

void alt_cache_l2_set_locks(uint32 cpus_di, uint32 ways);
// This function will assume that the instructions are not already in the cache
void alt_cache_l2_preload_instructions(void *where, uint32 size);

void alt_cache_l2_load2ways(void *address, uint32 size, uint32 way);

// This is a blocking call until the ways are clean
void alt_cache_l2_clean_cache_by_way( uint32 ways);
void alt_cache_l2_clean_invalidate_cache_by_way( uint32 ways);
void alt_cache_l2_invalidate_cache_by_way( uint32 ways);

#endif
