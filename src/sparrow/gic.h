/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#if !defined(GIC_H)
#define GIC_H
#include "core.h"

void gic_init_dist(void);
void gic_init_cpu(void);

#define GIC_CPUID_0			1
#define GIC_CPUID_1			2
#define GIC_CPUID_CURRENT_CPU		(2<<8)
#define GIC_CPUID_ALL_BUT_CURRENT	(1<<8)


void cpu_disable_ints(void);
void cpu_enable_ints(void);
#endif
