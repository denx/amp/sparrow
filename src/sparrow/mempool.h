/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
// VERY simple mempool - no freeing back, no segmentation allowed
#if !defined(MEMPOOL_H)
#define MEMPOOL_H

#include "core.h"
#include "alt_mmu.h"

typedef struct _MemPool
{
    uint32 start;
    uint32 end;
} MemPool;

void MemPoolInit(MemPool *pool, uint32 memloc, uint32 size);

void *MemPoolAlloc(const size_t size, void * pool);

static ALT_MMU_DAP_t g_domain_ap[16] = {
        ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER,
        ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER,
        ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER,
        ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER, ALT_MMU_DAP_MANAGER};
#endif
