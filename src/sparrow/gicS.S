/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include "asm.h"

    AREA(gicS, CODE, READONLY)
    EXPORT(cpu_disable_ints)
LABEL(cpu_disable_ints)
  mrs     r0, CPSR
  orr     r0, r0, #0x80
  msr     CPSR_c, r0
  bx      lr

    EXPORT(cpu_enable_ints)
LABEL(cpu_enable_ints)
  mrs     r0, CPSR
  bic     r0, r0, #0x80
  msr     CPSR_c, r0
  bx      lr

    END
