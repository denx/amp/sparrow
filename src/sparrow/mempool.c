/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
// VERY simple mempool - no freeing back, no segmentation allowed
#include "mempool.h"

void MemPoolInit(MemPool *pool, uint32 memloc, uint32 size)
{
    pool->start = memloc;
    pool->end = memloc + size;
}

void *MemPoolAlloc(const size_t size, void * context)
{
    MemPool *pool = (MemPool *)context;
    uint32 retval;
    uint32 align = size;

    if(!pool)
        return NULL;
    if(pool->start + size > pool->end)
        return NULL;
    retval = (pool->start + align-1) & ~(align-1);
    
    pool->start = retval + size;

    return (void *) (native_int) retval;
}


