/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#if !defined(ALT_PRINTF_H)
#define ALT_PRINTF_H
//#include <stdio.h>
#include "core.h"
#include <stdarg.h>

#if defined(ARMCOMPILER)
#define printf printf_function
#endif

typedef struct _FILEOP
{
  void (*print_function)(void *context, char toprint);
  void *context;
} FILEOP;
typedef FILEOP FILE;

void *memcpy(void *to, const void *from, size_t size);
int printf(const char *format, ...);
int fprintf(FILEOP *stream, const char *format, ...);

extern FILEOP term0_st;
extern FILEOP term1_st;

#define term0 ((FILE *) &term0_st)
#define term1 ((FILE *) &term1_st)

#if defined (PRINTF_HOST) || defined (PRINTF_UART)
#define ALT_PRINTF(...) printf(__VA_ARGS__)
#else
#define ALT_PRINTF(...) (void)(0)
#endif

#endif //ALT_PRINTF_H
