/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/
#include <core.h>

void _div0(void)
{}

void __attribute__ ((noinline)) ret01(unsigned int r0, unsigned int r1)
{}

void __attribute__ ((noinline)) retl01(uint64 r0, uint64 r1)
{}

static void intdiv(unsigned int num, unsigned int den, int issigned)
{
    int signNum = 0, signDen = 0;
    unsigned int result = 0, shifter=den;

    if(den == 0)
    {
        _div0(); // Error out here;
        return; 
    }

    if(issigned)
    {
        if(num & 0x80000000 )
        {
            signNum=1;
            num = -num;
        }
        if(den & 0x80000000 )
        {
            signDen = 1;
            den = -den;
        }
    }

    while(((shifter & 0xF0000000) == 0) &&
            shifter < num)
      shifter <<= 4;
    while(((shifter & 0x80000000) == 0) &&
            shifter < num)
      shifter <<= 1;
    while(shifter >= den)
    {
        result <<= 1;
        if(num >= shifter)
        {
            num -= shifter;
            result |= 1;
        }
        shifter >>= 1;
    }
    if(signNum)
        num = -num;
    if(signNum^signDen)
        result = -result;
    ret01(result, num);
}

void __aeabi_uidiv(unsigned int num, unsigned int den)
{
    intdiv(num, den, 0);
}

void __aeabi_uidivmod(unsigned int num, unsigned int den)
{
    intdiv(num, den, 0);
}

void __aeabi_idiv(int num, int den)
{
    intdiv(num, den, 1);
}

void __aeabi_idivmod(int num, int den)
{
    intdiv(num, den, 1);
}

// 64 bit versions
int x;

static void longdiv(uint64 num, uint64 den, int issigned)
{
    int signNum = 0, signDen = 0;
    uint64 result = 0, shifter;

    if(issigned)
    {
        if(num & 0x8000000000000000)
        {
            signNum=1;
            num = -num;
        }
        if(den & 0x8000000000000000)
        {
            signDen = 1;
            den = -den;
        }
    }
    if(den == 0)
    {
        _div0(); // Error out here;
    }
    shifter=den;
    while(((shifter & 0xFF00000000000000) == 0) &&
            shifter < num)
      shifter <<= 8;
    while(((shifter & 0x8000000000000000) == 0) &&
            shifter < num)
      shifter <<= 1;
    while(shifter >= den)
    {
        result <<= 1;
        if(num >= shifter)
        {
            num -= shifter;
            result |= 1;
        }
        shifter >>= 1;
    }
    if(signNum)
        num = -num;
    if(signNum^signDen)
        result = -result;
    retl01(result,num);
}

void __aeabi_uldiv(uint64 num, uint64 den)
{
    longdiv(num, den, 0);
}

void __aeabi_uldivmod(uint64 num, uint64 den)
{
    longdiv(num, den, 0);
}

void __aeabi_ldiv(uint64 num, uint64 den)
{ 
    longdiv(num, den, 1);
}

void __aeabi_ldivmod(uint64 num, uint64 den)
{
    longdiv(num, den, 1);
}
