#/******************************************************************************
#   Copyright Altera Corporation (C) 2015. All rights reserved
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms and conditions of the GNU General Public License,
#   version 2, as published by the Free Software Foundation.
#
#   This program is distributed in the hope it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along with
#   this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Based on the work of Jim Rucker at Altera Corporation
#
#******************************************************************************/

#####################			 Sample Code for AMP
setPhysicalMemory(1*Gb)

sparrow.setMapped(True)
sparrow.setCPU(1)

linux = Context("Linux", Secure=False, CreateMap=False, cpuNum=0)

MemoryRegion(
	Name    = "Linux's MR",
	Address = 0,
	MapTo	= [ Map(linux)],
	LoadFiles = [File(0x100, "socfpga.dtb", "dtb"), 
			File(0x7fc0, "uImage", "uimage", linux, 0x40)])

MemoryRegion(
	Name = "Memory for Baremetal App",
	Size = 1*Mb,
	MapTo = [Map(sparrow, AddrMatch=True)],
	LoadFiles = [File(0x0, "sparrow.bin", "sparrow", sparrow, 0x0)],
	StackFor = cpu1,
	MemFor = cpu0)

MemoryRegion(
        Name	= "Shared Comm Channel",
	Size	= 32*Mb,
	MapTo	= [Map(sparrow, Shared=True, NoCache=True, Device=True, AddrMatch=True),
		   Map(linux)])

MemoryRegion(
	Name	= "OS1's Log Buffer",
	Size	= 1*Mb,
	MapTo	= [Map(sparrow, Shared=True, NoCache=True, Device=True, AddrMatch=True),
		   Map(linux)])

MemoryRegion(
        Name    = "Peripherals",
        Address = 0xFF200000,
        Size    = 0x00E00000,
        MapTo   = [Map(sparrow, Device=True, NoCache=True, AddrMatch=True)])
