/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#ifndef __ALT_AMP_MSG_H__
#define __ALT_AMP_MSG_H__

#ifdef __cplusplus
extern "C"
{
#endif  /* __cplusplus */

typedef struct ALT_AMP_MSGHDR_s
{
	int32_t			msgtype;	/* encodes message type */
	uint32_t		length;		/* Size of the total message, in words */
} ALT_AMP_MSGHDR_t;


#define		ALT_AMP_MSG_STATUS_OK			(0)
#define		ALT_AMP_MSG_STATUS_CHECK		(1)
#define		ALT_AMP_MSG_STATUS_FAIL			(-1)
#define		ALT_AMP_MSG_STATUS_CONFIG_OK		(2)
#define		ALT_AMP_MSG_GETCONFIG			(3)
#define		ALT_AMP_MSG_WRITECONFIG			(4)
#define		ALT_AMP_MSG_TO_TERMINAL			(5)

#ifdef __cplusplus
}
#endif  /* __cplusplus */
#endif  /* __ALT_AMP_MSG_H__ */
