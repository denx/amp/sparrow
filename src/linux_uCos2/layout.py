#/******************************************************************************
#   Copyright Altera Corporation (C) 2015. All rights reserved
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms and conditions of the GNU General Public License,
#   version 2, as published by the Free Software Foundation.
#
#   This program is distributed in the hope it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along with
#   this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Based on the work of Jim Rucker at Altera Corporation
#
#******************************************************************************/

#####################			 Sample Code for AMP
setDevice("cyclone5")

setPhysicalMemory(1*Gb)

linux = Context("Linux", Secure=False, CreateMap=False, cpuNum=0)
uCos2 = Context("uCosii", Secure=True, CreateMap=False, cpuNum=1)

MemoryRegion(
	Name    = "Linux's MR",
	Address = 0,
	MapTo	= [ Map(linux)],
	LoadFiles = [	File(0x100, "socfpga.dtb", "dtb"), 
			File(0x7fc0, "uImage", "uimage", linux, 0x40)])

MemoryRegion(
	Name	= "uCos2 Memory",
	Size	= 1*Mb,
	MapTo	= [ Map(uCos2, AddrMatch=True)],
	LoadFiles = [File(0x0, "ucosii.bin", "uCos2", uCos2)],
	MemFor  = uCos2,
	StackFor = cpu1)

MemoryRegion(
        Name	= "Shared Comm Channel",
		# for OpenMCAPI memory is 2x16 + 1Mb 
	Size	= 33*Mb,
	MapTo	= [Map(uCos2, Shared=True, NoCache=True, Device=True, AddrMatch=True),
		   Map(linux, Shared=True)])

MemoryRegion(
	Name	= "OS1's Log Buffer",
	Size	= 1*Mb,
	MapTo	= [Map(uCos2, Shared=True, NoCache=True, Device=True, AddrMatch=True),
		   Map(linux, Shared=True)])

linux.addComponents(
                privateInts + globalTimer + legacyFIQ + privateTimer + legacyNIRQ + cpu0_parityfail + cpu1_parityfail +
		scu_parityfail + l2_ecc + ddr_ecc_error + dma + emac0 + emac1 + 
		usb0 + usb1 + 
		can0 + can1 +
		sdmmc + nand + qspi + spis + i2cs + 
		uart0 + uart1 + gpios + 
		timers + watchDogs +
		clkmgr + mpuwakeup + CoreSight + OCR + rom + sdram + dap + reset + sysmngr + scanmngr + stmslave)

linux.addComponent(
	Component(
		Name	= "cpu1_to_cpu0",
		Interrupts = [7]))
	

uCos2.addComponent(
	Component(
		Name	= "cpu0_to_cpu1",
		Interrupts = [8]))

uCos2.addComponent(
	Component(
		Name = "FPGA_Dev",
		Interrupts = [72, 73]))

