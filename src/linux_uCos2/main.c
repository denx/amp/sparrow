/******************************************************************************
   Copyright Altera Corporation (C) 2015. All rights reserved

   This program is free software; you can redistribute it and/or modify it
   under the terms and conditions of the GNU General Public License,
   version 2, as published by the Free Software Foundation.

   This program is distributed in the hope it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program.  If not, see <http://www.gnu.org/licenses/>.

   Based on the work of Jim Rucker at Altera Corporation

******************************************************************************/

#include <stdio.h>
#include "core.h"
#include "Layout.h"
#include "reset.h"
#include "cache.h"
#include "gic.h"
#include "alt_interrupt.h"

#define VERSION "0.6"

void cpu1_main(void);

int cpu0_main(void)
{
  uint32 clrmem;

  // Initialize the Shared Memory
  clrmem = System_Shared_Comm_Channel_Address;
  while(clrmem < System_Shared_Comm_Channel_Address + 0x1000)
                //System_Shared_Comm_Channel_AddrEnd)
  {
    *((uint32 *)clrmem) = 0;
    clrmem += 4;
  }

  // disable l2 cache to CPU0 initially. Linux will need to turn it on when it is ready
  alt_cache_l2_set_locks(CPU0_INSTRUCTION | CPU0_DATA, WAY0_7);

  // CPU1 instructions will into WAY0 or WAY1
  alt_cache_l2_set_locks(CPU1_INSTRUCTION, WAY2|WAY3|WAY4_7);

  // Don't let CPU1's data effect the instructions cached
  alt_cache_l2_set_locks(CPU1_DATA, WAY0);

  // enable l2 cache but not the L1
  alt_cache_l2_init();
  alt_cache_l2_prefetch_enable();
  alt_cache_l2_parity_enable();
  alt_cache_l2_enable();

  // Note: Although the l2 cache is enabled, instructions and data will not be cached unless
  // they are in virtual memory with the pages marked as cachable. When the MMU is turned off
  // it will not store any entries to the l2 cache.

  printf("Sparrow v "VERSION"\n");

  SetupTZPeripherals();

  wakeup_CPU(1, cpu1_main);

  SetupTZMemAccess();

  printf("Starting Linux on Core 0....\n\n");

  cpu0_OS_Start();

  return 0;
}

void cpu1_main(void)
{
//  Map_uCosii();
  cpu1_OS_Start();
}

//      These should match the driver(s) in Linux
#define TRIGGER_INT             1
#define DISABLE_L2_CACHE_CPU0   2
#define ENABLE_L2_CACHE_CPU0    3

void smc_handler(int Reason)
{
  switch(Reason)
  {
    case TRIGGER_INT:
      alt_int_sgi_trigger((ALT_INT_INTERRUPT_t)cpu0_to_cpu1_Interrupt,
        ALT_INT_SGI_TARGET_LIST,
        GIC_CPUID_1, 0);
      break;
    case DISABLE_L2_CACHE_CPU0:
      // diable CPU0's access to all ways
      alt_cache_l2_set_locks(CPU0_INSTRUCTION | CPU0_DATA, WAY0_7);
      break;
    case ENABLE_L2_CACHE_CPU0:
      // enable CPU0's access to L2 ways 2-7, leaving way 0-1 to CPU 1
      alt_cache_l2_set_locks(CPU0_INSTRUCTION | CPU0_DATA, WAY0 | WAY1);
      break;
  }
}

#ifndef GNU
int main(void)
{ return 0;}
#endif

